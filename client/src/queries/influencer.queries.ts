import { gql } from "@apollo/client";
import { GQLClient } from "../ApolloClient";

export const getInfluencerByUserId = (userId?: string) =>
  GQLClient.query({
    query: gql`
      query MyQuery($userId: String = "") {
        influencer(where: { user_id: { _eq: $userId } }) {
          bio
          category_name
          followers_count
          full_name
          id
          instagram_handle_name
          instagram_profile_link
          profile_pic
          user_id
          influencer_user {
            email
            id
            password
            phone_number
            type
          }
        }
      }
    `,
    fetchPolicy: "no-cache",
    variables: {
      userId,
    },
  });

export const getInfluencerTopCities = (userId?: string) =>
  GQLClient.query({
    query: gql`
      query MyQuery($userId: uuid = "") {
        influencer_top_cities(where: { influencer_id: { _eq: $userId } }) {
          influencer_top_cities_city_type {
            name
          }
        }
      }
    `,
    variables: {
      userId,
    },
  }).then((res) => {
    const l =
      res.data.influencer_top_cities.length > 3
        ? 3
        : res.data.influencer_top_cities.length;
    return res.data.influencer_top_cities
      .slice(0, l)
      .map((c) => c.influencer_top_cities_city_type.name)
      .join(" | ");
  });
