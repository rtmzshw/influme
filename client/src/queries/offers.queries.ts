import { gql } from "@apollo/client";
import { GQLClient } from "../ApolloClient";

export const getAllOffers = () =>
  GQLClient.query({
    query: gql`
      query OffersQuery {
        offer {
          id
          offer_influencer_offers {
            influencer_offer_influencer {
              id
              full_name
              profile_pic
              instagram_handle_name
            }
          }
          businessPartDetails
          influencerPartDetails
          maxAge
          maxOfferReach
          minAge
          name
          offer_business {
            category_name
            id
            name
            profile_pic
          }
          offer_offer_top_cities {
            offer_top_cities_city_type {
              id
              name
            }
          }
          offer_offer_genders {
            offer_gender_gender_type {
              id
              name
            }
          }
        }
      }
    `,
  });

export const getActiveOffersByBusinessId = (businessId: number) =>
  GQLClient.query({
    query: gql`
      query GetOffers($businessId: uuid!) {
        offer(
          where: {
            business_id: { _eq: $businessId }
            status: { _eq: "active" }
          }
        ) {
          id
          offer_influencer_offers {
            influencer_offer_influencer {
              id
              full_name
              instagram_handle_name
            }
          }
          businessPartDetails
          influencerPartDetails
          maxAge
          maxOfferReach
          minAge
          name
          imgUrl
          offer_business {
            category_name
            id
            name
            profile_pic
          }
          offer_offer_top_cities {
            offer_top_cities_city_type {
              id
              name
            }
          }
          offer_offer_genders {
            offer_gender_gender_type {
              id
              name
            }
          }
        }
      }
    `,
    variables: {
      businessId,
    },
  });

export const getById = (id?: string) =>
  GQLClient.query({
    query: gql`
      query OfferById($offerId: uuid!) {
        offer(where: { id: { _eq: $offerId } }) {
          id
          offer_influencer_offers {
            influencer_offer_influencer {
              id
              full_name
              profile_pic
              instagram_handle_name
            }
          }
          businessPartDetails
          influencerPartDetails
          maxAge
          maxOfferReach
          minAge
          imgUrl
          name
          offer_business {
            category_name
            id
            name
            profile_pic
          }
          offer_offer_top_cities {
            offer_top_cities_city_type {
              id
              name
            }
          }
          offer_offer_genders {
            offer_gender_gender_type {
              id
              name
            }
          }
        }
      }
    `,
    variables: {
      offerId: id,
    },
  });

export const deleteById = (id?: string) =>
  GQLClient.mutate({
    mutation: gql`
      mutation MyMutation($id: uuid!) {
        delete_offer_gender(where: { offer_id: { _eq: $id } }) {
          affected_rows
        }
        delete_offer_top_cities(where: { offer_id: { _eq: $id } }) {
          affected_rows
        }
        delete_influencer_relevant_offers(where: { offer_id: { _eq: $id } }) {
          affected_rows
        }
        delete_influencer_offer(where: { offer_id: { _eq: $id } }) {
          affected_rows
        }
        delete_offer(where: { id: { _eq: $id } }) {
          affected_rows
        }
      }
    `,
    variables: {
      id: id,
    },
  });

export const addNewOffer = (
  offerName: string,
  businessId: string | undefined,
  maxOfferReach: number,
  businessPartDetails: string,
  influencerPartDetails: string,
  minAge: number,
  maxAge: number,
  imgUrl: string
) =>
  GQLClient.mutate({
    mutation: gql`
      mutation MyMutation(
        $businessId: uuid!
        $businessPartDetails: String!
        $influencerPartDetails: String!
        $maxAge: Int!
        $maxOfferReach: Int!
        $minAge: Int!
        $name: String!
        $imgUrl: String!
      ) {
        insert_offer(
          objects: {
            businessPartDetails: $businessPartDetails
            influencerPartDetails: $influencerPartDetails
            maxAge: $maxAge
            maxOfferReach: $maxOfferReach
            minAge: $minAge
            name: $name
            business_id: $businessId
            imgUrl: $imgUrl
          }
        ) {
          returning {
            id
          }
        }
      }
    `,
    variables: {
      businessId: businessId,
      businessPartDetails,
      influencerPartDetails,
      maxOfferReach: maxOfferReach,
      name: offerName,
      minAge: minAge,
      maxAge: maxAge,
      imgUrl: imgUrl,
    },
  })
    .then((result) => {
      const offerId = result.data.insert_offer.returning[0].id;
      return offerId;
    })
    .catch((error) => {
      console.error("Failed to add offer:", error);
    });

export const addOfferCity = (city_id: string, offer_id: string) =>
  GQLClient.mutate({
    mutation: gql`
      mutation MyMutation($city_id: uuid!, $offer_id: uuid!) {
        insert_offer_top_cities(
          objects: { city_id: $city_id, offer_id: $offer_id }
        ) {
          affected_rows
        }
      }
    `,
    variables: {
      city_id: city_id,
      offer_id: offer_id,
    },
  });

export const addOfferGender = (gender_id: string, offer_id: string) =>
  GQLClient.mutate({
    mutation: gql`
      mutation MyMutation($gender_id: uuid!, $offer_id: uuid!) {
        insert_offer_gender(
          objects: { gender_id: $gender_id, offer_id: $offer_id }
        ) {
          affected_rows
        }
      }
    `,
    variables: {
      gender_id: gender_id,
      offer_id: offer_id,
    },
  });

export const updateOfferStatus = (id: string, status: string) =>
  GQLClient.mutate({
    mutation: gql`
      mutation MyMutation($id: uuid = "", $status: String = "") {
        update_offer_by_pk(pk_columns: { id: $id }, _set: { status: $status }) {
          id
        }
      }
    `,
    variables: {
      id: id,
      status: status,
    },
  });

export const getMaxOfferReachById = (offer_id: string) =>
  GQLClient.query({
    query: gql`
      query GetMaxOfferReach($offer_id: uuid) {
        offer(where: { id: { _eq: $offer_id } }) {
          maxOfferReach
        }
      }
    `,
    variables: {
      offer_id,
    },
  });

export const getOfferExposureByBusiness = (allOffersIds: string[]) =>
  GQLClient.query({
    query: gql`
      query GetOfferExposure($allOffersIds: [uuid] = []) {
        influencer_offer(where: { offer_id: { _in: $allOffersIds } }) {
          views
          likes
          comments
        }
      }
    `,
    variables: {
      allOffersIds,
    },
  });
