import { gql } from "@apollo/client";
import { GQLClient } from "../ApolloClient";
import { User } from "../models/user";
import { Business } from "../models/business";
import { Audience, Influencer, InfluencerTopCity } from "../models/influencer";

export const saveNewUser = (user: User) =>
  GQLClient.mutate({
    mutation: gql`
      mutation MyMutation(
        $email: String = ""
        $password: String = ""
        $type: String = ""
        $phoneNumber: String = ""
      ) {
        insert_user_one(
          object: {
            email: $email
            password: $password
            type: $type
            phone_number: $phoneNumber
          }
        ) {
          id
          type
        }
      }
    `,
    variables: {
      password: user?.password,
      email: user?.email,
      type: user?.type,
      phoneNumber: user?.phoneNumber,
    },
  });

export const saveNewUserProvider = (user: User) =>
  GQLClient.mutate({
    mutation: gql`
      mutation MyMutation(
        $id: String = ""
        $type: String = ""
        $phoneNumber: String = ""
        $email: String
      ) {
        insert_user_one(
          object: {
            id: $id
            type: $type
            email: $email
            phone_number: $phoneNumber
          }
        ) {
          id
          type
        }
      }
    `,
    variables: {
      id: user?.id,
      type: user?.type,
      phoneNumber: user?.phoneNumber,
      email: user?.email,
    },
  });

export const saveNewBusiness = (business: Business) =>
  GQLClient.mutate({
    mutation: gql`
      mutation MyMutation(
        $bio: String = ""
        $category_name: String = ""
        $followers_count: Int = 10
        $instagram_handle_name: String = ""
        $name: String = ""
        $profile_pic: String = ""
        $user_id: String = ""
      ) {
        insert_business_one(
          object: {
            bio: $bio
            category_name: $category_name
            followers_count: $followers_count
            instagram_handle_name: $instagram_handle_name
            name: $name
            profile_pic: $profile_pic
            user_id: $user_id
          }
        ) {
          id
        }
      }
    `,
    variables: {
      bio: business?.bio,
      category_name: business?.categoryName,
      followers_count: business?.followersCount,
      instagram_handle_name: business?.instagramHandleName,
      name: business?.name,
      profile_pic: business?.profilePic,
      user_id: business?.user?.id,
    },
  });

export const saveNewInfluencer = (influencer: Influencer) =>
  GQLClient.mutate({
    mutation: gql`
      mutation MyMutation(
        $bio: String = ""
        $category_name: String = ""
        $followers_count: Int = 10
        $full_name: String = ""
        $instagram_handle_name: String = ""
        $instagram_profile_link: String = ""
        $profile_pic: String = ""
        $user_id: String = ""
      ) {
        insert_influencer_one(
          object: {
            bio: $bio
            category_name: $category_name
            followers_count: $followers_count
            full_name: $full_name
            instagram_handle_name: $instagram_handle_name
            instagram_profile_link: $instagram_profile_link
            profile_pic: $profile_pic
            user_id: $user_id
          }
        ) {
          id
        }
      }
    `,
    variables: {
      bio: influencer?.bio,
      category_name: influencer?.categoryName,
      followers_count: influencer?.followersCount,
      full_name: influencer?.fullName,
      instagram_handle_name: influencer?.instagramHandleName,
      instagram_profile_link: influencer?.instagramProfileLink,
      profile_pic: influencer?.profilePic,
      user_id: influencer?.user?.id,
    },
  });

export const saveNewInfluencerAudience = (audience: Audience) =>
  GQLClient.mutate({
    mutation: gql`
      mutation MyMutation(
        $influencer_id: uuid = ""
        $max_followers_age: Int = 10
        $men_percentage: Int = 10
        $min_followers_age: Int = 10
        $woman_percentage: Int = 10
      ) {
        insert_audience_one(
          object: {
            influencer_id: $influencer_id
            max_followers_age: $max_followers_age
            men_percentage: $men_percentage
            min_followers_age: $min_followers_age
            woman_percentage: $woman_percentage
          }
        ) {
          influencer_id
        }
      }
    `,
    variables: {
      influencer_id: audience.influencerId,
      max_followers_age: audience.maxFollowersAge,
      min_followers_age: audience.minFollowersAge,
      men_percentage: audience.menPercentage,
      woman_percentage: audience.womanPercentage,
    },
  });

export const saveNewInfluencerCities = (cities: InfluencerTopCity[]) =>
  GQLClient.mutate({
    mutation: gql`
      mutation MyMutation($objects: [influencer_top_cities_insert_input!]!) {
        insert_influencer_top_cities(objects: $objects) {
          returning {
            city_id
            influencer_id
          }
        }
      }
    `,
    variables: {
      objects: cities.map(({ cityId, influencerId }) => ({
        city_id: cityId,
        influencer_id: influencerId,
      })),
    },
  });
