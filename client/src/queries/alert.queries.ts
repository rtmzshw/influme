import { gql } from "@apollo/client";
import { GQLClient } from "../ApolloClient";

export const getInfluencerAlertsDB = (influencerId: string) =>
  GQLClient.query({
    query: gql`
    query MyQuery {
      user_by_pk(id: "${influencerId}") {
        user_influencers {
          id
          influencer_influencer_relevant_offers(where: {is_alert: {_eq: true}}) {
            offer {
              id
              name
              imgUrl
            }
          }
        }
      }
    }   
    `,
    fetchPolicy: "no-cache"
  });

  export const setAlertsAsSeen = (userId: string) => 
  GQLClient.mutate({
    mutation: gql`
    mutation MyMutation($user_id: String!) {
      update_influencer_relevant_offers(
        where: { influencer: { user_id: {_eq: $user_id} } },
        _set: { is_alert: false }
      ) {
        affected_rows
      }
    }
    `,
    variables: {
      user_id: userId,
    },
  });