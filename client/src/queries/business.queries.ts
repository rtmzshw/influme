import { gql } from "@apollo/client";
import { GQLClient } from "../ApolloClient";

export const getBusinessByUserId = (userId?: string) =>
  GQLClient.query({
    query: gql`
    query MyQuery($userId: String = "") {
      business(where: {user_id: {_eq: $userId}}) {
        bio
        category_name
        followers_count
        id
        instagram_handle_name
        name
        profile_pic
        user_id
        business_user {
          email
          id
          password
          phone_number
          type
        }
      }
    }    
    `,
    fetchPolicy: "no-cache",
    variables: {
      userId,
    },
  });
