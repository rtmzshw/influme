import { gql } from "@apollo/client";
import { GQLClient } from "../ApolloClient";
import { uniqBy } from "ramda";
import { Business, businessMapper } from "../models/business";
import { Influencer, influencerMapper } from "../models/influencer";

export const getInfluencerOffersById = (influencerId: string) => GQLClient.query({
  query: gql`
  query InfluencerOffersQuery($influencerId: String = "") {
    influencer_offer(where: { influencer_id: { _eq: $influencerId } }) {
      influencer_id
      offer_id
    }
  }
  `,
  variables: {
    influencerId,
  },
});

export const getInfluencerOnGoingOffersById = (influencerId: string) => GQLClient.query({
  query: gql`
  query MyQuery($influencerId: uuid!) {
    influencer_offer(where: {influencer_id: {_eq: $influencerId}, status: {_neq: "approved"}}) {
      influencer_offer_offer {
        imgUrl
        influencerPartDetails
        name
      }
    }
  }  
  `,
  variables: {
    influencerId,
  },
}).then(res => res.data.influencer_offer.map(({influencer_offer_offer}) => influencer_offer_offer));

export const getInfluencerBusinessHistory = (influencerId: string) => GQLClient.query({
  query: gql`
  query BusinessHistroy($influencerId: uuid = "") {
    influencer_offer(where: {influencer_id: {_eq: $influencerId}, status: {_eq: "approved"}}) {
      influencer_offer_offer {
        offer_business {
          bio
          category_name
          followers_count
          id
          instagram_handle_name
          name
          profile_pic
          user_id
          business_user {
            email
            id
            password
            phone_number
            type
          }
        }
      }
    }
  }`,
  variables: {
    influencerId,
  },
}).then(res => {
  const businesses = res.data.influencer_offer.map(b => businessMapper(b.influencer_offer_offer.offer_business)) as Business[];
  return {
    all: businesses,
    uniq: uniqBy((b) => (b as any).id, businesses)
  };
});

export const getBusinessInfluencersHistory = (businessId: string) => GQLClient.query({
  query: gql`
  query MyQuery($businessId: uuid!) {
    influencer_offer(where: {status: {_eq: "approved"}, influencer_offer_offer: {business_id: {_eq: $businessId}}}) {
      influencer_offer_influencer {
        bio
        category_name
        followers_count
        full_name
        id
        instagram_profile_link
        instagram_handle_name
        profile_pic
        user_id
        influencer_user {
          email
          id
          password
          phone_number
          type
        }
      }
    }
  }`,
  variables: {
    businessId,
  },
}).then(res => {
  const influencers = res.data.influencer_offer.map(({ influencer_offer_influencer }) => influencerMapper(influencer_offer_influencer)) as Influencer[];
  return {
    all: influencers,
    uniq: uniqBy((i) => (i as any).id, influencers)
  };
});
