import { gql } from "@apollo/client";
import { GQLClient } from "./ApolloClient";

export const getUserById = (id: string) =>
  GQLClient.query({
    query: gql`
      query MyQuery($id: String!) {
        user_by_pk(id: $id) {
          id
          email
          password
          type
          phone_number
        }
      }
    `,
    variables: { id },
  });

export const getUserByEmailPassword = (email: string, password: string) =>
  GQLClient.query({
    query: gql`
      query MyQuery($email: String = "", $password: String = "") {
        user(where: { email: { _eq: $email }, password: { _eq: $password } }) {
          id
          email
          password
          type
          phone_number
        }
      }
    `,
    variables: { email, password },
  });
