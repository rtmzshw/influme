export enum OfferStatus {
    NEW = "New", 
    DONE = "Done",
    NOT_AVAILABLE = "Not Available"
}