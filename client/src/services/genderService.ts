import { gql } from "@apollo/client";
import { GQLClient } from "../ApolloClient";

export const getGenders = () =>
  GQLClient.query({
    query: gql`
      query MyQuery {
        gender_type {
          id
          name
        }
      }
    `,
  });
