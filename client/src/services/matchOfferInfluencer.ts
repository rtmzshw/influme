import { gql } from "@apollo/client";
import { GQLClient } from "../ApolloClient";
import { GENDER } from "../consts/gender.consts";

export const matchOfferInfluencer = async ({
  offerId,
  minAge,
  maxAge,
  genders,
  topAudienceCities,
}: {
  offerId: string;
  minAge: number;
  maxAge: number;
  genders: string[];
  topAudienceCities: string[];
}) => {
  const whereQuery = generateWhereQuery(genders);
  try {
    const [res, res1] = await Promise.all([
      audienceQuery(minAge, maxAge, whereQuery),
      audienceCityQuery(topAudienceCities),
    ]);

    const influencerIds1 = res.data.audience.map(
      (item: any) => item.influencer_id
    );
    const influencerIds2 = res1.data.influencer_top_cities.map(
      (item: any) => item.influencer_id
    );

    const influencerIds = [...new Set([...influencerIds1, ...influencerIds2])];

    await insertIntoRelvantOffers(influencerIds, offerId);
  } catch (e) {
    console.log(e);
  }
};

export const generateWhereQuery = (offerSelectedGenders: string[]) => {
  let where: string[] = [];
  if (offerSelectedGenders.length === 1) {
    const gender = offerSelectedGenders[0];
    if (gender === GENDER.FEMALE) {
      where.push("woman_percentage: {_gte: 50}");
    } else {
      where.push("men_percentage: {_gte: 50}");
    }
  }
  where.push(
    `_or: [{ min_followers_age: { _gte: $minAge } } { max_followers_age: { _lte: $maxAge } }]`
  );

  return where.join(" ");
};

export const audienceQuery = (
  minAge: number,
  maxAge: number,
  whereStr: string
) =>
  GQLClient.query({
    query: gql`
      query MyQuery($minAge: Int = 0, $maxAge: Int = 1000) {
        audience(
          where: {
            ${whereStr}
          }
        ) {
          influencer_id
        }
      }
    `,
    variables: { minAge, maxAge },
  });

export const audienceCityQuery = (cityTypeIds: (string | undefined)[]) =>
  GQLClient.query({
    query: gql`
      query MyQuery($_in: [uuid!] = []) {
        influencer_top_cities(where: { city_id: { _in: $_in } }) {
          influencer_id
        }
      }
    `,
    variables: { _in: cityTypeIds },
  });

export const insertIntoRelvantOffers = (
  influencerIds: string[],
  offerId: string
) =>
  GQLClient.mutate({
    mutation: gql`
      mutation MyMutation(
        $objects: [influencer_relevant_offers_insert_input!]!
      ) {
        insert_influencer_relevant_offers(objects: $objects) {
          returning {
            influencer_id
            offer_id
          }
        }
      }
    `,
    variables: {
      objects: influencerIds.map((influencerId) => ({
        influencer_id: influencerId,
        offer_id: offerId,
        start_date: new Date().toISOString(),
        status: "New",
        status_change_date: new Date().toISOString(),
        is_alert: true
      })),
    },
  });
