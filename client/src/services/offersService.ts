import { Offer, offerMapper } from "../models/offer";
import { getAllOffers, getById, addNewOffer, addOfferCity, addOfferGender, updateOfferStatus, getActiveOffersByBusinessId, deleteById, getOfferExposureByBusiness } from "../queries/offers.queries";


export type QueryFunction = () => Promise<any>;

const executeQuery = async (queryFunction: QueryFunction) => {
  try {
    const response = await queryFunction();
    return response;
  } catch (error) {
    console.error("Failed to fetch offers:", error);
    throw error;
  }
};

export const getOffers = async (): Promise<Offer[]> => (await executeQuery(getAllOffers)).data.offer.map((x: any) => offerMapper(x));

export const getBusinessOffers = async (businessId: number): Promise<Offer[]> => (await executeQuery(() => getActiveOffersByBusinessId(businessId))).data.offer.map((x: any) => offerMapper(x));

export interface Exposure {
  views: number;
  comments: number;
  likes: number;
}

export const getBusinessExposureFromOffers = async (allOffers: Offer[]): Promise<Exposure> => {
const allOffersIds = allOffers.map(offer => offer.id);

  return ((await executeQuery(() => getOfferExposureByBusiness(allOffersIds))).data?.influencer_offer as any[])?.reduce((sum, curr) => ({
  views: sum.views + curr.views,
  comments: sum.comments + curr.comments,
  likes: sum.likes + curr.likes
}), { views: 0, likes: 0, comments: 0 })};

export const getOfferById = async (id :string): Promise<Offer> => offerMapper((await executeQuery(() => getById(id))).data.offer[0])

export const deleteOfferById = async (id? :string): Promise<string> => await executeQuery(() => deleteById(id))

export const addOffer = async (offerName: string,
  businessId: string | undefined,
  maxOfferReach: number,
  businessPartDetails: string,
  influencerPartDetails: string,
  minAge: number,
  maxAge: number,
  imgUrl: string): Promise<string> => await executeQuery(() => addNewOffer(offerName, businessId, maxOfferReach, businessPartDetails, influencerPartDetails, minAge, maxAge, imgUrl))

export const addOfferTopCity = async (city_id: string, offer_id: string): Promise<void> => await executeQuery(() => addOfferCity(city_id, offer_id))

export const addOfferTopGender = async (gender_id: string, offer_id: string): Promise<void> => await executeQuery(() => addOfferGender(gender_id, offer_id))

export const updateOfferStatusById = async (id: string, status: string): Promise<void> => await executeQuery(() => updateOfferStatus(id, status))