import { gql } from "@apollo/client"
import { GQLClient } from "../ApolloClient"
import { IAlert, alertMapper } from "../models/alert"
import { getInfluencerAlertsDB } from "../queries/alert.queries"
import { navigate } from "@reach/router"

export type QueryFunction = () => Promise<any>

const executeQuery = async (queryFunction: QueryFunction) => {
  try {
    const response = await queryFunction()
    return response
  } catch (error) {
    console.error("Failed to fetch alerts:", error)
    throw error
  }
}

export const getInfluencerAlerts = async (
  influencer_id: string
): Promise<IAlert[]> =>
  (
    await executeQuery(() => getInfluencerAlertsDB(influencer_id))
  ).data.user_by_pk.user_influencers[0].influencer_influencer_relevant_offers.map(
    (alert: any) => alertMapper(alert)
  )

export const getBusinessAlerts = async (userId: string) =>
  (
    await GQLClient.query({
      query: gql`
      query MyQuery {
        influencer_offer(where: {status: {_eq: "wating-for-approval"}, influencer_offer_offer: {offer_business: {business_user: {id: {_eq: "${userId}"}}}}}) {
          offer_id
          influencer_id
          influencer_offer_offer {
            name
            imgUrl
          }
        }
      }
      `,
      fetchPolicy: "no-cache"
    })
  ).data
  .influencer_offer.map((response) => {
    const action = () => navigate(`approveWork/${response.offer_id}/${response.influencer_id}`)
    const {name,imgUrl} = response.influencer_offer_offer
    return {actionName: "Wating for your approval", imgUrl, name, action} as IAlert
  })