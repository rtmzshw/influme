import { gql } from "@apollo/client";
import { GQLClient } from "../ApolloClient";
import { GENDER } from "../consts/gender.consts";

export const findRelevantOffers = async ({
  influencerId,
  minAge,
  maxAge,
  womanPercentage,
  topAudienceCities,
}: {
  influencerId: string;
  minAge: number;
  maxAge: number;
  womanPercentage: number;
  topAudienceCities: string[];
}) => {
  try {
    const [res, res1, res2] = await Promise.all([
      ageQuery(minAge, maxAge),
      cityQuery(topAudienceCities),
      genderQuery(womanPercentage),
    ]);

    const offerIds1 = res.data.offer.map((item: any) => item.id);
    const offerIds2 = res1.data.offer_top_cities.map(
      (item: any) => item.offer_id
    );
    const offerIds3 = res2.data.offer_gender.map((item: any) => item.offer_id);

    const offerIds = [...new Set([...offerIds1, ...offerIds2, ...offerIds3])];

    await insertIntoRelvantOffers(influencerId, offerIds);
  } catch (e) {
    console.log(e);
  }
};

export const ageQuery = (minAge: number, maxAge: number) =>
  GQLClient.query({
    query: gql`
      query MyQuery($minAge: Int = 0, $maxAge: Int = 1000) {
        offer(
          where: {
            maxAge: { _lte: $maxAge }
            _or: { minAge: { _gte: $minAge } }
          }
        ) {
          id
        }
      }
    `,
    variables: { minAge, maxAge },
  });

export const cityQuery = (cityTypeIds: (string | undefined)[]) =>
  GQLClient.query({
    query: gql`
      query MyQuery($_in: [uuid!] = []) {
        offer_top_cities(where: { city_id: { _in: $_in } }) {
          offer_id
        }
      }
    `,
    variables: { _in: cityTypeIds },
  });

export const genderQuery = (womanPercentage: number) => {
  let genderId: string = GENDER.MALE;
  if (womanPercentage >= 50) genderId = GENDER.FEMALE;

  return GQLClient.query({
    query: gql`
      query MyQuery($genderId: uuid!) {
        offer_gender(where: { gender_id: { _eq: $genderId } }) {
          offer_id
        }
      }
    `,
    variables: { genderId },
  });
};

export const insertIntoRelvantOffers = (
  influencerId: string,
  offerIds: string[]
) =>
  GQLClient.mutate({
    mutation: gql`
      mutation MyMutation(
        $objects: [influencer_relevant_offers_insert_input!]!
      ) {
        insert_influencer_relevant_offers(objects: $objects) {
          returning {
            influencer_id
            offer_id
          }
        }
      }
    `,
    variables: {
      objects: offerIds.map((offerId) => ({
        influencer_id: influencerId,
        offer_id: offerId,
        start_date: new Date().toISOString(),
        status: "New",
        status_change_date: new Date().toISOString(),
        is_alert: true
      })),
    },
  });
