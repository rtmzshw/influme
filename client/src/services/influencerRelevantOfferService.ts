import { gql } from "@apollo/client";
import { GQLClient } from "../ApolloClient";
import { OfferStatus } from "../consts/offerStatus";

export const updateNewRelevantOfferStatus = (
  influencer_id: string,
  offer_id: string,
  status: OfferStatus,
  status_change_date: Date
) =>
  GQLClient.mutate({
    mutation: gql`
      mutation MyMutation(
        $influencer_id: uuid,
        $offer_id: uuid,
        $status: String!,
        $status_change_date: date
      ) {
        update_influencer_relevant_offers(
          where: {
            influencer_id: { _eq: $influencer_id },
            offer_id: { _eq: $offer_id }
            status: { _eq: ${OfferStatus.NEW} }
          },
          _set: {
            status: $status,
            status_change_date: $status_change_date
          }
        ) {
          affected_rows
        }
      }
    `,
    variables: {
      influencer_id,
      offer_id,
      status,
      status_change_date: status_change_date.toISOString().substr(0, 10),
    },
  });

export const getInfluencersAmountThatImplementedOffer = (offer_id: string) =>
  GQLClient.query({
    query: gql`
      query MyQuery($offer_id: uuid) {
        influencer_relevant_offers(
          where: { offer_id: { _eq: $offer_id }, status: { _eq: "Done" } }
        ) {
          influencer_id
        }
      }
    `,
    variables: {
      offer_id,
    },
  });

export const getInfluencerRelevantOffers = (influencer_id: number) =>
  GQLClient.query({
    query: gql`
      query MyQuery($influencer_id: uuid) {
        influencer_relevant_offers(
          where: {
            influencer_id: { _eq: $influencer_id }
            status: { _eq: "New" }
          }
        ) {
          offer {
            id
            offer_influencer_offers {
              influencer_offer_influencer {
                id
                full_name
                instagram_handle_name
              }
            }
            businessPartDetails
            influencerPartDetails
            maxAge
            maxOfferReach
            minAge
            imgUrl
            name
            offer_business {
              category_name
              id
              name
              profile_pic
            }
            offer_offer_top_cities {
              offer_top_cities_city_type {
                id
                name
              }
            }
            offer_offer_genders {
              offer_gender_gender_type {
                id
                name
              }
            }
          }
        }
      }
    `,
    variables: {
      influencer_id,
    },
  });

export const updateOfferStatusToNotAvailable = (
  offer_id: string,
  status_change_date: Date
) =>
  GQLClient.mutate({
    mutation: gql`
    mutation MyMutation(
        $offer_id: uuid,
        $status_change_date: date
      ) {
        update_influencer_relevant_offers(
          where: {
            offer_id: { _eq: $offer_id }
            status: { _eq: ${OfferStatus.NEW} }
          }
          _set: {
            status: "${OfferStatus.NOT_AVAILABLE}"
            status_change_date: $status_change_date
          }
        ) {
          affected_rows
        }
      }
      
    `,
    variables: {
      offer_id,
      status_change_date: status_change_date.toISOString().substr(0, 10),
    },
  });
