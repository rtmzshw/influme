import { gql } from "@apollo/client";
import { GQLClient } from "../ApolloClient";
import { City } from "../models/city";

export const getAllCities = () => GQLClient.query({
  query: gql`
  query MyQuery {
    city_type(order_by: { name: asc }) {
      id
      name
    }
  }
  `,
});


export const initCities = (cities: City[]) =>{
  return GQLClient.mutate({
    mutation: gql`
    mutation InsertCities($objects: [city_type_insert_input!]!) {
      insert_city_type(objects: $objects) {
        returning {
          id
          name
        }
      }
    }
    `,
    variables: {
      objects: cities.map(({ name }) => ({
        name
      })),
    },
  })}

  const cityTypeIds = [
    '2671b7d8-216e-4bc5-ad23-47734f9398ef',
    'c2e41105-6a19-4eef-a786-e964487a9c4d',
    'a30795a2-845f-432f-82cd-be2809480d3e'
  ];
  
export const deleteCities = () =>{
  return GQLClient.mutate({
    mutation: gql`
    mutation MyMutation($_in: [uuid!] = []) {
      delete_city_type(where: { _not: { id: { _in: $_in } } }) {
        returning {
          id
        }
      }
    }  
    `,
    variables: {
      _in: cityTypeIds,
    },
  })}
