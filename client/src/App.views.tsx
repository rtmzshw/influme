import React from "react";
import { View } from "./App.types";
import MainPage from "./container/landingPage/mainPage";
import WorkHistoryPage from "./pages/History";
import HomePage from "./pages/Home";
import Login from "./pages/login/login";
import NewOffer from "./pages/NewOffer";
import AlertsPage from "./pages/Alerts";
import OfferDetails from "./pages/OfferDetails";
import ProfilePage from "./pages/Profile";
import SignUpIntro from "./pages/register/SignUpIntro";
import RegisterStepper from "./pages/register/stepper/components/RegisterStepper";
import ProveWork from "./pages/ProveWork";
import ApproveWork from "./pages/ApproveWork";
import BusinessProfilePage from "./pages/BusinessProfile";
import NotFoundPage from "./pages/NotFound";

export const VIEWS: View[] = [
  {
    path: "/landing",
    component: <MainPage key="MainPage" path="landing" />,
  },
  {
    path: "/login",
    component: <Login key="Login" path="login" />,
  },
  {
    path: "/register",
    component: <RegisterStepper key="register" path="register" />,
  },
  {
    path: "/register2",
    component: <SignUpIntro key="register2" path="register2" />,
  },
  {
    path: "/proveWork/:offerId",
    component: <ProveWork key="ProveWork" path="proveWork/:offerId" />,
  },
  {
    path: "/register3",
    component: <RegisterStepper key="RegisterStepper" path="register3" />,
  },

];

export const NAVBAR_VIEW: View[] = [
  {
    path: "/app/home",
    component: <HomePage key="HomePage" path="/app/home" />,
  },
  {
    path: "/app/notFound",
    component: <NotFoundPage key="NotFoundPage" path="/app/notFound" />,
  },
  {
    path: "/app/profile/User/:userId",
    component: <ProfilePage key="ProfilePage" path="/app/profile/User/:userId" />,
  },
  {
    path: "/app/profile/Business/:userId",
    component: <BusinessProfilePage key="BusinessProfilePage" path="/app/profile/Business/:userId" />,
  },
  {
    path: "/app/profile/business",
    component: <BusinessProfilePage key="BusinessProfilePage" path="/app/profile/business" />,
  },
  {
    path: "/app/history",
    component: <WorkHistoryPage key="WorkHistoryPage" path="/app/history" />,
  },
  {
    path: "/app/offer/:id",
    component: <OfferDetails key="OfferDetails" path="/app/offer/:id" />,
  },
  {
    path: "/app/newOffer",
    component: <NewOffer key="NewOffer" path="/app/newOffer" />,
  },
  {
    path: "/app/alerts",
    component: <AlertsPage key="AlertsPage" path="/app/alerts" />,
  },
  {
    path: "/app/approveWork/:offerId/:influencerId",
    component: (
      <ApproveWork
        key="ApproveWork"
        path="/app/approveWork/:offerId/:influencerId"
      />
    ),
  },
];
