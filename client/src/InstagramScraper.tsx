import axios from "axios"

export interface InstgramScaperData {
  handleName?: string,
  instagramProfileLink?: string,
  biography?: string
  full_name?: string
  followersCount?: number
  followCount?: number
  is_business_account?: boolean
  is_professional_account?: boolean
  business_email?: string
  business_phone_number?: string
  business_category_name?: string
  overall_category_name?: string
  category_enum?: string
  category_name?: string
  is_private?: boolean
  is_verified?: boolean
  profile_pic_url?: string
  profile_pic_url_hd?: string
}
export const getUserInstgramData = async (username: string) => {
  const options = {
    method: "GET",
    url: "https://instagram130.p.rapidapi.com/account-info",
    params: {
      username,
    },
    headers: {
      "X-RapidAPI-Key": "51904fd7e7mshde3f64689cbfc34p155df8jsnb82f7c8a9874",
      "X-RapidAPI-Host": "instagram130.p.rapidapi.com",
    },
  }

  try {
    const response = await axios.request(options)
    const instgramData: InstgramScaperData = {
      biography: response.data.biography,
      full_name: response.data.full_name,
      followersCount: response.data.edge_followed_by?.count,
      followCount: response.data.edge_follow?.count,
      is_business_account: response.data.is_business_account,
      is_professional_account: response.data.is_professional_account,
      business_email: response.data.business_email,
      business_phone_number: response.data.business_phone_number,
      business_category_name: response.data.business_category_name,
      overall_category_name: response.data.overall_category_name,
      category_enum: response.data.category_enum,
      category_name: response.data.category_name,
      is_private: response.data.is_private,
      is_verified: response.data.is_verified,
      profile_pic_url: response.data.profile_pic_url,
      profile_pic_url_hd: response.data.profile_pic_url_hd,
    }
    return instgramData
  } catch (error) {
    console.error(error)
  }
}
