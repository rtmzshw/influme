import { CircularProgress, makeStyles } from "@material-ui/core"
import { Redirect, RouteComponentProps, Router } from "@reach/router"
import { prop } from "ramda"
import React, { useEffect, useState } from "react"
import { NAVBAR_VIEW, VIEWS } from "./App.views"
import Navbar from "./component/Navbar"
import { useLoggedUserContext } from "./pages/context/loggedUser"
import { getInfluencerByUserId } from "./queries/influencer.queries"
import { getBusinessByUserId } from "./queries/business.queries"
import { getBusinessAlerts, getInfluencerAlerts } from "./services/alertService"
import { setAlertsAsSeen } from "./queries/alert.queries"
const useStyles = makeStyles((theme) => ({
  root: {
    height: "100vh",
    width: "100vw",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
}))

const StartApp = (props: RouteComponentProps) => {
  const classes = useStyles()
  const { user, setUser, setUserType, reloadUser, setReloadUser, userType, reloadNotifactions ,setAlerts } =
    useLoggedUserContext()
  const [loading, setLoader] = useState(true)
  const [initalView, setView] = useState(VIEWS[0])
  const [notifications, setNotifications] = useState<any[] | null>(null)

  const getLoggedUserData = () => {
    const cookies = document.cookie.split("; ")
    const loggedUserCookie = cookies.find((cookie) =>
      cookie.startsWith("loggedUser=")
    )

    if (loggedUserCookie) {
      const loggedUserValue = loggedUserCookie.split("=")[1]
      const loggedUserData = JSON.parse(loggedUserValue)
      return loggedUserData
    }

    return null
  }

  useEffect(() => {
    const fetchUser = async () => {
      const loggedUser: any = getLoggedUserData()

      if (!loggedUser) {
        setLoader(false)
        setUser(null)
        setUserType("")
        setAlerts([])
        setReloadUser(false)
        setView(VIEWS[0])
        return
      }

      setUserType(loggedUser.type)

      if (loggedUser.type === "User") {
        const [res, currAlerts] = await Promise.all([
          getInfluencerByUserId(loggedUser?.id),
          getInfluencerAlerts(loggedUser?.id),
        ])
        await setAlertsAsSeen(loggedUser?.id)
        setUser(res.data.influencer[0])
        setNotifications(currAlerts)
        setAlerts(currAlerts)
      } else {
        const [res, currAlerts] = await Promise.all([
          getBusinessByUserId(loggedUser?.id),
          getBusinessAlerts(loggedUser?.id),
        ])
        setUser(res.data.business[0])
        setNotifications(currAlerts)
        setAlerts(currAlerts)
      }

      setView(NAVBAR_VIEW[0])
      setLoader(false)
    }

    fetchUser()
  }, [reloadUser])

  const getNotifactions = async () => {
    const loggedUser: any = getLoggedUserData()
    if (loggedUser?.userType === "User") {
      const currAlerts = await getInfluencerAlerts(loggedUser?.id)
      await  setAlertsAsSeen(loggedUser?.id)
      setNotifications(currAlerts)
      setAlerts(currAlerts)
    } else {
      const  currAlerts =  await  getBusinessAlerts(loggedUser?.id)
      setNotifications(currAlerts)
      setAlerts(currAlerts)
    }
  }

  useEffect(() => {
    getNotifactions()
  }, [reloadNotifactions])

  return (
    <div>
      {loading ? (
        <div className={classes.root}>
          <CircularProgress />
        </div>
      ) : (
        <Router>
          <Redirect noThrow from='/' to={initalView.path} />
          {VIEWS.map(prop("component"))}
          <Navbar default path='/app' notifications={notifications} />
        </Router>
      )}
    </div>
  )
}

export default StartApp
