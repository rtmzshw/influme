import { makeStyles, Paper, Typography } from "@material-ui/core";
import clsx from "clsx";
import React from "react";
import { navigate } from "@reach/router";
import { IAlert } from "../models/alert";
import { useLoggedUserContext } from "../pages/context/loggedUser";

export interface IAlertProps {
  // alert: IAlert;
}

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    alignItems: "center",
    backgroundColor: theme.palette.secondary.main,
    borderRadius: "5px",
    width: "80%",
    padding: "5px",
    marginTop: "10px",
  },
  center: {
    display: 'flex',
    alignItems: "center",
    padding: '6px 20px 6px 20px',
    boxShadow: "0px 2px 4px 0px #bebebe",
    background: 'white',
  },
  img: {
    height: "90px",
    width: "90px",
    minWidth: "90px",
    padding: "5px"
  },
}));

const Alert = (props: { alert: IAlert }) => {
  const classes = useStyles();
  const { setReloadNotifactions, reloadNotifactions } =
  useLoggedUserContext()
  // const { alert } = props;

  const navigateAction = (action: any) => {
    setReloadNotifactions(!reloadNotifactions);
    action()
  }

  return (
    <div className={classes.center} onClick={() => navigateAction(props.alert.action)}>
      <img className={classes.img} src={props.alert.imgUrl} />
      <div style={{ width: '8px' }}></div>
      <span>
        <Typography style={{ fontSize: "20px", fontWeight: 500 }}>{props.alert.actionName}</Typography>
        <Typography style={{ fontSize: "16px" }}>{props.alert.name}</Typography>
      </span>
    </div>
  );
};

export default Alert;
