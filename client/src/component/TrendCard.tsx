import { makeStyles } from "@material-ui/core";
import React from "react";

interface TrendCardProps {
  count: number;
  trend: string;
  icon: JSX.Element;
}

const useStyles = makeStyles((theme) => ({
  paper: {
    // backgroundColor: theme.palette.secondary.main,
    backgroundColor: "white",
    display: "flex",
    alignItems: "center",
    boxShadow: "none",
    textAlign: "center",
    padding: "10px",
    marginLeft: "15px",
    borderRadius: "10px",
    height: "30%",
  },
  text: {
    fontWeight: 600,
    paddingLeft: "8px",
    fontSize: "14px",
  },
  num: {
    color: theme.palette.primary.main,
    fontWeight: 500,
    fontSize: "20px",
  },
}));

const TrendCard = (props: TrendCardProps) => {
  const classes = useStyles();
  const { count, trend, icon } = props;
  return (
    <div
      style={{ display: "flex", alignItems: "center", flexDirection: "column" }}
    >
      <span className={classes.num}>{count}</span>
      <div style={{ display: "flex", alignItems: "center" }}>
        {icon}
        <span className={classes.text}>{trend}</span>
      </div>
    </div>
  );
};

export default TrendCard;
