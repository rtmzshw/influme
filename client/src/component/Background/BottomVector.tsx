import { makeStyles } from "@material-ui/core";
import React from "react";

const useStyles = makeStyles((theme) => ({
    root: {
        position: "absolute",
        bottom: 0,
        right: 0,
        zIndex: -100
    }
}));

const BottomVector = () => {
    const classes = useStyles();

    return (
        <svg className={classes.root} width="200" height="363" viewBox="0 0 200 363" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M448.414 176.652C449.106 224.403 452.613 267.247 430.067 286.508C407.307 305.508 358.494 300.926 324.419 313.484C290.132 325.782 270.811 355.035 236.523 374.297C202.676 393.635 154.088 402.797 116.577 384.208C79.0652 365.618 53.0702 319.353 31.1123 271.536C9.36767 223.981 -8.11238 174.688 5.84973 139.794C19.5986 104.639 64.5622 84.0681 98.95 61.6774C133.125 39.0253 156.923 15.2611 187.13 5.85631C217.565 -3.73397 254.395 1.48212 299.428 6.53723C344.673 11.8537 398.121 17.0092 423.776 46.1401C449.432 75.271 447.508 128.639 448.414 176.652Z" fill="#6B4EFF" fill-opacity="0.2" />
        </svg>
    );
}

export default BottomVector;