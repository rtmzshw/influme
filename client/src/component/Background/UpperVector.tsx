import { makeStyles } from "@material-ui/core";
import React from "react";

const useStyles = makeStyles((theme) => ({
    root: {
        position: "absolute",
        top: -50,
        zIndex: -100
    }
}));

const UpperVector = () => {
    const classes = useStyles();

    return (
        <svg className={classes.root} width="375" height="348" viewBox="0 0 375 348" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M469.844 102.765C474.899 161.86 482.899 215.137 457.548 236.954C431.918 258.429 372.938 248.445 333.161 260.956C293.105 273.125 272.509 307.581 233.069 328.361C194.164 349.273 136.672 356.301 90.0144 330C43.3571 303.699 8.07024 244.203 -22.5092 183.146C-52.8098 122.43 -78.1467 59.9438 -64.48 18.0409C-51.0923 -24.2038 1.04249 -45.6561 40.3257 -70.2945C79.3299 -95.2749 105.784 -122.548 141.2 -131.502C176.872 -140.666 221.529 -130.958 276.014 -120.725C330.777 -110.149 395.369 -99.0459 428.732 -60.7624C462.095 -22.4788 464.509 43.328 469.844 102.765Z" fill="#6B4EFF" fill-opacity="0.2" />
        </svg>
    );
}

export default UpperVector;