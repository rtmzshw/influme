import { makeStyles } from "@material-ui/core";
import React from "react";

const useStyles = makeStyles((theme) => ({
    root: {
        position: "absolute",
        top: 0,
        zIndex: -100
    }
}));

const SmallUpperVector = () => {
    const classes = useStyles();

    return (
        <svg className={classes.root} width="375" height="157" viewBox="0 0 375 157" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M409.844 -81.8224C414.899 -140.917 422.899 -194.194 397.548 -216.012C371.918 -237.487 312.938 -227.502 273.161 -240.014C233.105 -252.183 212.509 -286.638 173.069 -307.418C134.164 -328.33 76.6718 -335.358 30.0144 -309.057C-16.6429 -282.757 -51.9298 -223.26 -82.5092 -162.203C-112.81 -101.487 -138.147 -39.0011 -124.48 2.90168C-111.092 45.1465 -58.9575 66.5987 -19.6743 91.2371C19.3299 116.218 45.7839 143.491 81.1998 152.445C116.872 161.608 161.529 151.901 216.014 141.667C270.777 131.091 335.369 119.989 368.732 81.705C402.095 43.4214 404.509 -22.3853 409.844 -81.8224Z" fill="#6B4EFF" fill-opacity="0.2" />
        </svg>

    );
}

export default SmallUpperVector;