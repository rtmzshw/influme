import { makeStyles } from "@material-ui/core";
import React from "react";
import BottomVector from "./BottomVector";
import UpperVector from "./UpperVector";

interface Props {
    children: JSX.Element
}

const useStyles = makeStyles((theme) => ({
    
}));

const Background = (props: Props) => {
    const classes = useStyles();

    return (
        <div>
            <UpperVector/>
            {props.children}
            <BottomVector/>
        </div>
    );
}

export default Background;