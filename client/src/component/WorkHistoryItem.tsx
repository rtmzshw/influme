import { makeStyles, Typography } from "@material-ui/core";
import clsx from "clsx";
import React from "react";
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import { ProfileData } from "../App.types";
import { navigate } from "@reach/router";

export interface WorkHistoryItemProps extends ProfileData {
    //TODO: remove
    imgsrc?: string;
}

const useStyles = makeStyles((theme) => ({
    root: {
        display: "flex",
        alignItems: "center",
        backgroundColor: theme.palette.secondary.main,
        borderRadius: "5px",
        width: "80%",
        padding: "5px",
        marginTop: "15px"
    },
    details: {
        alignSelf: "flex-start",
        paddingTop: "5px",
        height: "90%",
        width: "65%",
        position: "relative"
    },
    text: {
        fontSize: "12px",
        fontWeight: 400,
        paddingLeft: "5px"
    },
    img: {
        height: "90px",
        width: "90px",
        minWidth: "90px",
        padding: "5px"
    },
    profileLink: {
        position: "absolute",
        bottom: 0,
        right: 0,
        display: "flex",
        alignItems: "center",
        color: theme.palette.primary.main,
        fontWeight: 800
    }
}));

const WorkHistoryItem = (props: WorkHistoryItemProps) => {
    const classes = useStyles();
    const { _id, category, profileLocation, profileName, imgsrc } = props;

    return (
        <div className={classes.root}>
            <img className={classes.img} src={imgsrc} />
            <span className={classes.details}>
                <Typography className={classes.text} style={{ fontSize: "18px" }}>{profileName}</Typography>
                <Typography className={classes.text}>{profileLocation}</Typography>
                <Typography className={classes.text}>Associated w/ {category}</Typography>
                <Typography className={clsx(classes.text, classes.profileLink)} onClick={e => navigate(`/app/profile/${_id}`)}>
                    Watch Profile
                    <ArrowForwardIcon color="primary" fontSize="small" />
                </Typography>
            </span>
        </div>
    );
}

export default WorkHistoryItem;