import {
  AppBar,
  Badge,
  IconButton,
  makeStyles,
  Toolbar,
} from "@material-ui/core"
import HomeOutlinedIcon from "@material-ui/icons/HomeOutlined"
import NotificationsNoneIcon from "@material-ui/icons/NotificationsNone"
import PersonOutlineIcon from "@material-ui/icons/PersonOutline"
import SearchIcon from "@material-ui/icons/Search"
import { navigate, Redirect, RouteComponentProps, Router } from "@reach/router"
import { prop } from "ramda"
import React, { useContext, useState } from "react"
import { NAVBAR_VIEW } from "../App.views"
import { useLoggedUserContext } from "../pages/context/loggedUser"
import { OfferProvider } from "../pages/context/offer/offerProvider"
import SearchBar from "./SearchBar"
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { OfferContext } from "../pages/context/offer/offerContext"
import { IAlert } from "../models/alert"

interface navbarProps extends RouteComponentProps {
  notifications?: IAlert[] | null
}

const useStyles = makeStyles((theme) => ({
  root: {
    background: "none",
    boxShadow: "none",
    height: "8vh",
  },
  toolbar: {
    display: "flex",
    justifyContent: "space-between",
  },
  exitIcon: {
    transform: "rotate(180deg)",
  },
}))

const Navbar = (props: navbarProps) => {
  const classes = useStyles()
  const { notifications } = props
  const [showSearchInput, setShowSearchInput] = useState(false)
  const { user, userType, setUser, setUserType } = useLoggedUserContext();
  const offerContext = useContext(OfferContext);
  const profileLink = `/app/profile/${userType}/${user?.user_id}`

  const handleLogout = () => {
    document.cookie = "loggedUser=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    setUser(null);
    setUserType("");
    offerContext.setOffers([]);
    navigate("/login");
  };


  return (
    <div style={{ overflow: "auto" }}>
      <AppBar position='static' className={classes.root}>
        <Toolbar className={classes.toolbar}>
          <div>
            <IconButton size='small' onClick={handleLogout}>
              <ExitToAppIcon color='primary' className={classes.exitIcon} />
            </IconButton>
            <IconButton size='small' onClick={(e) => navigate("/app/alerts")}>
              <Badge badgeContent={notifications?.length} color='primary'>
                <NotificationsNoneIcon color='primary' />
              </Badge>
            </IconButton>
            <IconButton size='small' onClick={(e) => navigate(profileLink)}>
              <PersonOutlineIcon color='primary' />
            </IconButton>
          </div>
          {showSearchInput && (
            <SearchBar />
          )
          }
          <div>
            <IconButton size='small'>
              <SearchIcon color='primary' onClick={() => setShowSearchInput(!showSearchInput)} />
            </IconButton>
            <IconButton size='small' onClick={(e) => navigate("/app/home")}>
              <HomeOutlinedIcon color='primary' />
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
      <OfferProvider>
        <Router style={{ height: "92vh" }}>
          <Redirect noThrow from='/app' to={NAVBAR_VIEW[0].path} />
          {NAVBAR_VIEW.map(prop("component"))}
        </Router>
      </OfferProvider>
    </div>
  )
}

export default Navbar
