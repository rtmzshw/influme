import React, { useState } from "react";
import { Button, ButtonGroup, makeStyles } from "@material-ui/core";
import { Option } from "./MultipleSelect";

type genderButtonGroupProps = {
  genders: Option[];
  onSelectionChange?: (selectedOptions: string[]) => void; // New callback prop
};

const useStyles = makeStyles((theme) => ({
  buttonGroup: {
    height: "15%",
    marginTop: "15px",
    backgroundColor: "white"
  },
  selected: {
    backgroundColor: "red", // Update the background color here
  },
  icon: {
    width: 25,
    height: 25
  }
}));

const GenderButtonGroup = ({ genders, onSelectionChange }: genderButtonGroupProps) => {
  const classes = useStyles();
  const [selectedOptions, setSelectedOptions] = React.useState<string[]>([]);

  const handleChange = (selectedItem: string) => {
    const isCurrentlySelected = selectedOptions.includes(selectedItem);
    let updatedSelectedOptions;

    if (isCurrentlySelected) {
      updatedSelectedOptions = selectedOptions.filter((option) => option !== selectedItem);
    } else {
      updatedSelectedOptions = [...selectedOptions, selectedItem];
    }

    if (updatedSelectedOptions.length <= 5) {
      setSelectedOptions(updatedSelectedOptions);
      if (onSelectionChange) {
        onSelectionChange(updatedSelectedOptions);
      }
    }
  };

  const isGenderSelected = (genderId: string) => {
    return selectedOptions.includes(genderId);
  };

  return (
    <ButtonGroup className={classes.buttonGroup} size="large" aria-label="outlined secondary button group">
      {genders.map((gender) => (
        <Button
          key={gender.id}
          value={gender.id}
          onClick={() => handleChange(gender.id)}
          style={isGenderSelected(gender.id) ? { backgroundColor: "#EBEDEF" } : {}}
        >
          {gender.name}
        </Button>
      ))}
    </ButtonGroup>
  );
};

export default GenderButtonGroup;
