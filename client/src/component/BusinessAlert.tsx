import { makeStyles, Paper, Typography } from "@material-ui/core";
import clsx from "clsx";
import React from "react";
import { navigate } from "@reach/router";
import { IAlert } from "../models/alert";

export interface IAlertProps {
  alert: IAlert;
}

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    alignItems: "center",
    backgroundColor: theme.palette.secondary.main,
    borderRadius: "5px",
    width: "80%",
    padding: "5px",
    marginTop: "10px",
  },
  text: {
    fontSize: "20px",
    fontWeight: 400,
  },
  paper: {
    borderRadius: "30px",
    position: "relative",
    height: "15vh",
    width: "75%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    boxShadow: "0px 10px 20px rgba(0, 95, 183, 0.3)",
    margin: "10px",
    textAlign: "center",
    padding: "10px",
  },
  title: {
    fontSize: "18px",
    fontWeight: 700,
    color: theme.palette.primary.main,
  },
  details: {
    fontSize: "15px",
    fontWeight: 400,
    height: "35%",
  },
  profileLink: {
    marginTop: '12px',
    color: theme.palette.primary.main,
    fontWeight: 600,
  },
}));

const BusinessAlert = (props: { offerId?: string, influencerId?: string }) => {
  const classes = useStyles();
  // const { alert } = props;

  return (
    <Paper className={classes.paper}>
      <div>
        <span className={classes.title}>Offer</span>
        <br></br>
        <span className={classes.details}> New offer wating for approval</span>
      </div>
      <Typography
        className={clsx(classes.text, classes.profileLink)}
        onClick={(e) => navigate(`/approveWork/${props.offerId}/${props.influencerId}`)}
      >
        Watch prove image
      </Typography>
    </Paper>
  );
};

export default BusinessAlert;
