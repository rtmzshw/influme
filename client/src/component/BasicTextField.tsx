import React from "react";
import {
  TextField,
  makeStyles,
  TextFieldProps,
  InputAdornment,
} from "@material-ui/core";

type BasicTextFieldProps = TextFieldProps & {
  label: string;
  prefix?: string;
  suffix?: string; // New property for suffix
  error?: boolean | undefined; // Updated type for error prop
};

const useStyles = makeStyles((theme) => ({
  basicTextField: {
    width: "100%",
    marginTop: theme.spacing(2),
    background: "white",
    color: theme.palette.primary.main,
    borderColor: theme.palette.primary.main,
  },
}));

const BasicTextField = (props: BasicTextFieldProps) => {
  const { prefix, suffix, ...rest } = props;
  const classes = useStyles();

  return (
    <TextField
      variant="outlined"
      className={classes.basicTextField}
      {...rest}
      InputProps={{
        startAdornment: prefix && (
          <InputAdornment position="start">{prefix}</InputAdornment>
        ),
        endAdornment: suffix && (
          <InputAdornment position="end">{suffix}</InputAdornment>
        ),
      }}
    />
  );
};

export default BasicTextField;
