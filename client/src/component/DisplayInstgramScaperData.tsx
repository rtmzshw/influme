import React from "react"
import { Card, CardContent, Typography, makeStyles } from "@material-ui/core"
import { InstgramScaperData } from "../InstagramScraper"

interface DisplayInstgramScaperDataProps {
  data: InstgramScaperData
}

const useStyles = makeStyles((theme) => ({
  field: {
    marginBottom: theme.spacing(1),
  },
}))

const DisplayInstgramScaperData: React.FC<DisplayInstgramScaperDataProps> = ({
  data,
}) => {
  const classes = useStyles()

  return (
    <>
      <Typography className={classes.field}>
        <strong>Full Name:</strong> {data.full_name || "-"}
      </Typography>

      <Typography className={classes.field}>
        <strong>Biography:</strong> {data.biography || "-"}
      </Typography>

      <Typography className={classes.field}>
        <strong>Followers Count:</strong> {data.followersCount || 0}
      </Typography>

      {/* <Typography className={classes.field}>
        <strong>Follow Count:</strong> {data.followCount || 0}
      </Typography> */}

      {/* <Typography className={classes.field}>
        <strong>Is Business Account:</strong>{" "}
        {data.is_business_account ? "Yes" : "No"}
      </Typography>

      <Typography className={classes.field}>
        <strong>Is professional Account:</strong>{" "}
        {data.is_professional_account ? "Yes" : "No"}
      </Typography>
      <Typography className={classes.field}>
        <strong>Business email:</strong> {data.business_email}
      </Typography>
      <Typography className={classes.field}>
        <strong>Business phone number:</strong> {data.business_phone_number}
      </Typography>
      <Typography className={classes.field}>
        <strong>business category name:</strong> {data.business_category_name}
      </Typography>
      <Typography className={classes.field}>
        <strong>overall category name:</strong> {data.overall_category_name}
      </Typography> */}
      <Typography className={classes.field}>
        <strong>category name:</strong> {data.category_name}
      </Typography>
      {/* <Typography className={classes.field}>
        <strong>Is private:</strong> {data.is_private ? "Yes" : "No"}
      </Typography>
      <Typography className={classes.field}>
        <strong>Is verified:</strong> {data.is_verified ? "Yes" : "No"}
      </Typography> */}
    </>
  )
}

export default DisplayInstgramScaperData
