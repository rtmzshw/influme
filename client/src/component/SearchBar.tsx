import { makeStyles, TextField } from "@material-ui/core";
import React, {useContext, useState} from "react";
import { useSearchContext } from "../pages/context/searchContext";

const useStyles = makeStyles((theme) => ({
    searchBar: {
        width: "180px",
        '& .MuiInputBase-input': {
            height: '8px',
        },
        '& .MuiOutlinedInput-root': {
            '& fieldset': {
              borderColor: theme.palette.primary.main,
        }
    }
    }
}));

const SearchBar = () => {
    const classes = useStyles();
    const { searchText, onSearchChange } = useSearchContext();

    return (
        <TextField className={classes.searchBar} label="Search in Influme..." 
                                variant="outlined" value={searchText} onChange={onSearchChange}/>
    );
}

export default SearchBar;