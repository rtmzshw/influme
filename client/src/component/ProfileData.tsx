import { Divider, makeStyles, Typography } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import React from "react";
import LocationOnOutlinedIcon from '@material-ui/icons/LocationOnOutlined';
import BusinessCenterOutlinedIcon from '@material-ui/icons/BusinessCenterOutlined';
import { ProfileData as IProfileData } from "../App.types";
import { navigate } from "@reach/router";

interface profileDataProps extends Omit<IProfileData, '_id'> {
    businessAmount: number;
}

const useStyles = makeStyles((theme) => ({
    paper: {
        backgroundColor: theme.palette.secondary.main,
        borderRadius: "35px",
        display: "flex",
        alignItems: "center",
        width: "90%",
        height: "10%",
        boxShadow: "none",
        textAlign: "center",
    },
    details: {
        height: "75%",
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-around",
        padding: "10px"
    },
    text: {
        fontSize: "12px",
        fontWeight: 400,
        paddingLeft: "5px"
    }
}));

const ProfileData = (props: profileDataProps) => {
    const classes = useStyles();
    const { profileName, businessAmount, category, profileLocation } = props;
    return (
        <Paper className={classes.paper}>
            <span style={{ textAlign: "center", width: "35%", padding: "5px" }}>
                <Typography style={{ fontSize: "18px", fontWeight: 800 }}>{businessAmount}</Typography>
                <Typography className={classes.text}>Collaborations</Typography>
            </span>
            <Divider orientation="vertical" />
            <span className={classes.details}>
                {profileLocation != "" &&
                    <span style={{ display: "flex", alignItems: "center", textAlign: "start" }}>
                        <LocationOnOutlinedIcon />
                        <Typography className={classes.text}>{profileLocation}</Typography>
                    </span>
                }
                {category != "" &&
                    <span style={{ display: "flex", alignItems: "center", textAlign: "start" }}>
                        <BusinessCenterOutlinedIcon />
                        <Typography className={classes.text}>Associated w/ {category}</Typography>
                    </span>
                }
            </span>
        </Paper>
    );
}

export default ProfileData;