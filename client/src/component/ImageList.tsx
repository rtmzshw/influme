import { makeStyles, Typography } from "@material-ui/core";
import React from "react";

interface ImageListProps {
  images: { name: string; src: string, userId: string }[];
  imageStyle: React.CSSProperties;
  title: string;
  style?: React.CSSProperties;
  onClick: (userId: string) => void;
}

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    alignSelf: "end",
    width: "95%",
  },
  images: {
    overflow: "auto",
    width: "100%",
    height: "80%",
    display: "flex",
    alignItems: "center",
  },
}));

const ImageList = (props: ImageListProps) => {
  const classes = useStyles();
  const { imageStyle, images, title, style, onClick } = props;

  return (
    <div className={classes.root} style={style}>
      <Typography>{title}</Typography>
      <div className={classes.images}>
        {images.map(({ name, src, userId }) => (
          <img
            key={name}
            src={src}
            alt={name}
            style={imageStyle}
            onClick={() => onClick(userId)}
          />
        ))}
      </div>
    </div>
  );
};

export default ImageList;
