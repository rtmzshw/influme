import { makeStyles } from "@material-ui/core"
import Paper from "@material-ui/core/Paper"
import React from "react"

interface InfoCardProps {
  title: string
  details: string
  pic?: string;
  children?: JSX.Element;
  instaName?: string;
  instaLink?: string;
}

const useStyles = makeStyles((theme) => ({
  paper: {
    borderRadius: "30px",
    position: "relative",
    height: "25vh",
    width: "75%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    boxShadow: "0px 10px 20px rgba(0, 95, 183, 0.3)",
    margin: "20px",
  },
  title: {
    fontSize: "14px",
    fontWeight: 700,
  },
  subtitle: {
    fontSize: "12px",
    fontWeight: 600,
  },
  details: {
    fontSize: "12px",
    fontWeight: 400,
    height: "35%",
    padding: "4px",
    overflow: "hidden",
    textAlign: "center"
  },
}))

export const createPicComponent = (
  size: number,
  imgSrc?: string,
  style?: React.CSSProperties,
) => {
  const basicStyle = {
    border: "5px solid white",
    borderRadius: "50%",
    height: `${size}px`,
    width: `${size}px`,
  }
  return (
    <img
      style={{ ...basicStyle, ...style }}
      src={imgSrc ?? require("../assets/profilepic.png")}
    />
  )
}

const InfoCard = (props: InfoCardProps) => {
  const classes = useStyles()
  const { details, title, pic, children, instaName, instaLink } = props

  return (
    <Paper className={classes.paper}>
      {createPicComponent(90, pic, {
        position: "absolute",
        top: "-55px",
      })}
      <div style={{ height: "65px" }}></div>
      <span className={classes.title}>{title}</span>
      <a href={instaLink} target="_blank" rel="noopener noreferrer">
        <span className={classes.subtitle}>{`@${instaName}`}</span>
      </a>
      <span className={classes.details}>{details}</span>
      {children}
    </Paper>
  )
}

export default InfoCard
