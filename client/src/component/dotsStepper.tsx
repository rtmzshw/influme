import React, { useState } from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import MobileStepper from '@material-ui/core/MobileStepper';
import Button from '@material-ui/core/Button';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';

const useStyles = makeStyles({
  root: {
    maxWidth: 400,
    flexGrow: 1,
    backgroundColor: 'transparent ',
    paddingTop: '20px'
  },
  dot: {
    backgroundColor: 'grey',
  },
});

type DotsStepperProps = {
    currentStep: number;
    handleNext: () => void,
    handleBack: () => void
};

const DotsStepper = ({ currentStep, handleBack, handleNext }: DotsStepperProps) => {
  const classes = useStyles();
  const theme = useTheme();
  const stepNumber = 4;

  return (
    <MobileStepper
      variant="dots"
      steps={stepNumber}
      position="static"
      activeStep={currentStep}
      classes={{
        root: classes.root,
        dotActive: classes.dot,
      }}
      nextButton={
        <Button size="small" onClick={handleNext} disabled={currentStep === stepNumber - 1}>
          {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
        </Button>
      }
      backButton={
        <Button size="small" onClick={handleBack} disabled={currentStep === 0}>
          {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
        </Button>
      }
    />
  );
}

export default DotsStepper;