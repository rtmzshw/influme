import React, { ChangeEvent } from "react"
import FormControl from "@material-ui/core/FormControl"
import Select from "@material-ui/core/Select"
import MenuItem from "@material-ui/core/MenuItem"
import { InputLabel, createStyles, makeStyles } from "@material-ui/core"

interface SelectComponentProps {
  name: string
  label: string
  value: string
  options: string[]
  onChange: (event: ChangeEvent<{ name?: string; value: unknown }>) => void
}

const useStyles = makeStyles((theme) =>
  createStyles({
    formControl: {
      marginTop: theme.spacing(1),
      width: "100%",
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  })
)

const SelectField: React.FC<SelectComponentProps> = ({
  name,
  label,
  value,
  options,
  onChange,
}) => {
  const classes = useStyles()

  return (
    <FormControl variant='outlined' className={classes.formControl}>
      <InputLabel>{label}</InputLabel>
      <Select value={value} onChange={onChange} label={label} name={name}>
        <MenuItem value=''>
          <em>None</em>
        </MenuItem>
        {options.map((option) => (
          <MenuItem key={option} value={option}>
            {option}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  )
}

export default SelectField
