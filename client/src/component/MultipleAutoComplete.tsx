import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete, { AutocompleteProps } from '@material-ui/lab/Autocomplete';
import { Theme, createStyles, makeStyles } from '@material-ui/core';

type Option = {
  id: string;
  name: string;
};

type MultiSelectProps = {
  items: Option[];
  label: string;
  onChange: (selectedIds: string[]) => void;
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      marginTop: theme.spacing(2),
      width: 330,
    },
  }),
);

const MultipleAutoComplete: React.FC<MultiSelectProps> = ({ items, label, onChange }) => {
  const [selectedIds, setSelectedIds] = useState<string[]>([]);
  const classes = useStyles();

  const handleChange: AutocompleteProps<Option, boolean, boolean, boolean>['onChange'] = (
    _event: any,
    selectedOptions: any
  ) => {
    const newSelectedIds = selectedOptions?.map((option: Option) => option.id);
    if (newSelectedIds.length <= 5) {
      setSelectedIds(newSelectedIds);
      onChange(newSelectedIds);
    }
  };

  return (
    <Autocomplete
    className={classes.formControl}
    multiple
    options={items}
    getOptionLabel={(option: Option) => option.name}
    value={items.filter((option: Option) => selectedIds.includes(option.id))}
    onChange={handleChange}
    renderInput={(params: any) => (
      <TextField {...params} label={label} variant="outlined" style={{backgroundColor:"white"}}/>
    )}
    disableCloseOnSelect={selectedIds.length >= 5}
    disableClearable={true}
  />
  );
};

export default MultipleAutoComplete;
