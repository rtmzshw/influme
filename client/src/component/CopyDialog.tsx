import React from 'react';
import { Button, Dialog, DialogActions, DialogContent } from '@material-ui/core';

interface CopyDialogProps {
    text: string
    open: boolean
    setOpen: (b :boolean) => void
  }

const CopyDialog = ({ text, open, setOpen } : CopyDialogProps) => {

  const handleClose = () => {
    setOpen(false);
  };

  const handleCopy = () => {
    const el = document.createElement('textarea');
    el.value = text;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    handleClose();
  };

  return (
    <div>
      <Dialog open={open} onClose={handleClose}>
      <DialogContent>
          <textarea
            readOnly
            style={{ position: 'absolute', left: -9999 }}
            value={text}
            ref={(textarea) => textarea && textarea.select()}
          />
          {text}
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Close
          </Button>
          <Button onClick={handleCopy} color="primary">
            Copy
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default CopyDialog;
