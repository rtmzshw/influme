import React from "react";
import { Button, makeStyles } from "@material-ui/core";
import clsx from 'clsx';

interface gradientButtonProps {
    title: string;
    type: string;
    onClickFunc?: () => any;
}

const useStyles = makeStyles((theme) => ({
  basicButton: {
    textTransform: 'capitalize',
    borderRadius: '7px',
    fontSize: '1rem'
  },
  dark: {
    background: theme.palette.primary.main,
    color: 'white',
    borderColor: 'white',
    '&:hover': {
      background: 'white',
      color: theme.palette.primary.main,
      borderColor: theme.palette.primary.main
    }
  },
  light: {
    background: 'white',
    color: theme.palette.primary.main,
    borderColor: theme.palette.primary.main,
    '&:hover': {
      background: theme.palette.primary.main,
      color: 'white',
      borderColor: 'white'
    }
  }
}));

const BasicButton = ({title, type, onClickFunc}: gradientButtonProps) => {
  const classes = useStyles();

  return (
    <Button variant='outlined' className={clsx({
      [classes.basicButton] : true,
      [classes.dark] : type === 'dark',
      [classes.light] : type === 'light'
      })} onClick={onClickFunc}>{title}</Button>
  );
}

export default BasicButton;