import React from "react";
import { Button, ButtonProps, makeStyles, StyledComponentProps } from "@material-ui/core";
import clsx from 'clsx';

type BasicButtonProps = ButtonProps & {
  title: string;
  colorTheme: string;
  // onClickFunc?: (...args: any[]) => any;
  // style?: React.CSSProperties;
}

const useStyles = makeStyles((theme) => ({
  basicButton: {
    textTransform: 'capitalize',
    borderRadius: '7px',
    fontSize: '1rem',
    margin: '5px'
  },
  dark: {
    background: theme.palette.primary.main,
    color: 'white',
    borderColor: 'white',
    '&:hover': {
      background: 'white',
      color: theme.palette.primary.main,
      borderColor: theme.palette.primary.main
    }
  },
  light: {
    background: 'white',
    color: theme.palette.primary.main,
    borderColor: theme.palette.primary.main,
    '&:hover': {
      background: theme.palette.primary.main,
      color: 'white',
      borderColor: 'white'
    }
  },
  gradient: {
    color: 'white',
    border: "none",
    background: "linear-gradient(270deg, #6B4EFF 12.79%, #A594FF 87.28%)"
  }
}));

const BasicButton = (props: BasicButtonProps) => {
  const { colorTheme, title } = props;
  const classes = useStyles();

  return (
    <Button variant='outlined' {...props} className={clsx({
      [classes.basicButton]: true,
      [classes.dark]: colorTheme === 'dark',
      [classes.light]: colorTheme === 'light',
      [classes.gradient]: colorTheme === 'gradient'
    })}>{title}</Button>
  );
}

export default BasicButton;