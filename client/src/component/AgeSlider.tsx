import { Box, Slider, Typography, makeStyles } from "@material-ui/core";
import React, { useState } from "react";

const useStyles = makeStyles((theme) => ({
  box: {
    width: "75%",
    marginTop: "15px",
  },
  disabled: {
    cursor: "default",
    pointerEvents: "none",
    color: "rgba(107, 78, 255, 0.7)",
  },
}));

interface IAgeSliderOptionalProps {
  minAge: number;
  maxAge: number;
  disabled: boolean;
  onSliderChange?: (min: number, max: number) => void; // New callback prop
}

const defaultProps: IAgeSliderOptionalProps = {
  minAge: 0,
  maxAge: 100,
  disabled: false,
};

const AgeSlider = ({
  minAge,
  maxAge,
  disabled,
  onSliderChange,
}: IAgeSliderOptionalProps) => {
  const classes = useStyles();

  const [value, setValue] = useState<[number, number]>([minAge, maxAge]);

  const handleChange = (
    event: React.ChangeEvent<{}>,
    newValue: number | number[]
  ) => {
    const [newMin, newMax] = newValue as [number, number];
    setValue([newMin, newMax]);
    if (onSliderChange) {
      onSliderChange(newMin, newMax);
    }
  };

  const valuetext = (value: number) => {
    return `${value}`;
  };

  return (
    <Box className={classes.box}>
      <Typography>
        Age Range: {value[0]} - {value[1]}
      </Typography>
      <Slider
        className={disabled ? classes.disabled : ""}
        value={value}
        onChange={handleChange as (
          event: React.ChangeEvent<{}>,
          value: number | number[]
        ) => void}
        valueLabelDisplay="auto"
        getAriaValueText={valuetext}
      />
    </Box>
  );
};

AgeSlider.defaultProps = defaultProps;

export default AgeSlider;
