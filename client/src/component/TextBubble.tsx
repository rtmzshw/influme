import { makeStyles } from "@material-ui/core";
import { RouteComponentProps } from "@reach/router";
import clsx from 'clsx';
import "./TextBubble.css";

interface TextBubbleProps extends RouteComponentProps {
  businessPart: string;
  influencerPart: string;
}

const useStyles = makeStyles((theme) => ({
  primary: {
    backgroundColor: theme.palette.primary.main
  },
  secondary: {
    backgroundColor: theme.palette.secondary.main,
    color: "black"
  }
}));

const TextBubbles = ({ businessPart, influencerPart }: TextBubbleProps) => {
  const classes = useStyles();

  return (
    <div>
      <div className={clsx("bubble left", classes.secondary)}>
        <b>Business Part:</b>
        <br />
        {businessPart}
      </div>
      <div style={{ height: "20px" }} />
      <div className={clsx("bubble right", classes.primary)}>
        <b>Influencer Part:</b>
        <br />
        {influencerPart}
      </div>
    </div>
  );
}

export default TextBubbles;