import React from "react";
import { IconButton, IconButtonProps, makeStyles } from "@material-ui/core";
import clsx from 'clsx';

type BasicIconButtonProps = IconButtonProps & {
  colorTheme: string;
}

const useStyles = makeStyles((theme) => ({
  basicIconButton: {
    borderRadius: '7px',
    margin: '5px'
  },
  dark: {
    background: theme.palette.primary.main,
    '&:hover': {
      background: 'white',
      color: theme.palette.primary.main
    }
  },
  light: {
    color: theme.palette.primary.main,
    '&:hover': {
      background: theme.palette.primary.main,
      color: 'white'
    }
  },
  gradient: {
    color: 'white',
    border: "none",
    background: "linear-gradient(270deg, #6B4EFF 12.79%, #A594FF 87.28%)"
  }
}));

const BasicIconButton = (props: BasicIconButtonProps) => {
  const { colorTheme, children } = props;
  const classes = useStyles();

  return (
    <IconButton {...props} className={clsx({
      [classes.basicIconButton]: true,
      [classes.dark]: colorTheme === 'dark',
      [classes.light]: colorTheme === 'light',
      [classes.gradient]: colorTheme === 'gradient'
    })}>{children}</IconButton>
  );
}

export default BasicIconButton;
