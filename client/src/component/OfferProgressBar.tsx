import { makeStyles } from "@material-ui/core";
import LinearProgress from "@material-ui/core/LinearProgress";
import PermIdentityIcon from "@material-ui/icons/PermIdentity";
import React from "react";

interface OfferProgressBarProps {
  currentInfluencerReached: any[];
  maxOfferReached: number;
}

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    width: "100%"
  },
  flex: {
    display: "flex",
    height: "50%",
    paddingLeft: "4px",
    alignItems: "center",
  },
  progressBar: {
    width: "100%",
    height: "1vh",
    borderRadius: "10px",
    marginTop: "10px",
  },
  influencerPic: {
    border: "1px solid white",
    borderRadius: "50%",
    height: "20px",
    width: "20px",
    margin: "-4px",
  },
  images: {
    display: "flex",
    flexDirection: "row",
    marginLeft: "4px",
  },
  offerNumbers: {
    padding: "2px",
  },
}));

const OfferProgressBar = ({
  maxOfferReached,
  currentInfluencerReached,
}: OfferProgressBarProps) => {
  const classes = useStyles();
  const currentOfferReached = currentInfluencerReached.length;
  const progress = (currentOfferReached / maxOfferReached) * 100;

  return (
    <div className={classes.root}>
      <PermIdentityIcon color="primary" />
      <LinearProgress
        className={classes.progressBar}
        variant="determinate"
        value={progress}
      />
      <div className={classes.flex}>
        <span className={classes.offerNumbers}>
          {currentOfferReached}/{maxOfferReached}
        </span>
        {/* <div className={classes.images}>
          {currentInfluencerReached.slice(0, 3).map(({ profilePic }) => (
            <img className={classes.influencerPic} src={profilePic} alt={profilePic}/>
          ))}
        </div> */}
      </div>
    </div>
  );
};

export default OfferProgressBar;
