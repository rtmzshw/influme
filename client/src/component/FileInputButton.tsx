import { makeStyles } from "@material-ui/core"
import React, { useRef, useState } from "react"
import BasicButton from "./BasicButton"
interface FileInputButtonProps {
  label: string
  onChange: (file: string | null) => void
}

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
  },
  imgContainer: {
    width: "150px",
    height: "150px",
    borderRadius: "50%",
    overflow: "hidden",
    border: "2px solid black",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
}))

const FileInputButton: React.FC<FileInputButtonProps> = ({
  label,
  onChange,
}) => {
  const fileInputRef = useRef<HTMLInputElement>(null)
  const [filePreview, setFilePreview] = useState<string | null>(null)
  const classes = useStyles()

  const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0] || null

    if (file) {
      const reader = new FileReader()
      reader.onloadend = () => {
        setFilePreview(reader.result as string)
        onChange(reader.result as string)
      }
      reader.readAsDataURL(file)
    } else {
      setFilePreview(null)
    }
  }

  const handleButtonClick = () => {
    if (fileInputRef.current) {
      fileInputRef.current.click()
    }
  }

  return (
    <div className={classes.container}>
      {filePreview ? (
        <div className={classes.imgContainer}>
          <img
            src={filePreview}
            alt='File Preview'
            style={{ width: "100%", height: "100%" }}
          />
        </div>
      ) : (
        <div className={classes.imgContainer}></div>
      )}
      <input
        type='file'
        accept='image/*'
        ref={fileInputRef}
        onChange={handleFileChange}
        style={{ display: "none" }}
      />
      <BasicButton
        title={label}
        colorTheme='dark'
        key='signup'
        onClick={handleButtonClick}
      />
    </div>
  )
}

export default FileInputButton
