import { Typography, makeStyles } from "@material-ui/core";
import StoreIcon from '@material-ui/icons/Storefront'
import OfferProgressBar from "./OfferProgressBar";

interface OfferProps {
    bussinessName: string;
    name: string;
    details: string;
    maxOfferReached?: number;
    //TODO: Change to influencer model
    currentInfluencerReached?: any[];
    imgUrl: any;
    style?: any;
}

const useStyles = makeStyles((theme) => ({
    root: {
        display: "flex",
        alignItems: "center",
        backgroundColor: "white",
        borderRadius: "8px",
        marginTop: "15px",
        height: "85%",
        boxShadow: "0px 2px 4px 0px #bebebe"
    },
    details: {
        alignSelf: "flex-start",
        paddingTop: "5px",
        paddingLeft: "4px",
        height: "90%",
        width: "65%",
    },
    text: {
        fontSize: "12px",
        fontWeight: 400,
        paddingLeft: "5px"
    },
    img: {
        height: "100px",
        width: "90px",
        minWidth: "90px",
        borderRadius: "4px"
    },
    profileLink: {
        position: "absolute",
        bottom: 0,
        right: 0,
        display: "flex",
        alignItems: "center",
        color: theme.palette.primary.main,
        fontWeight: 800
    }
}));

const Offer = ({ name, details, maxOfferReached, currentInfluencerReached, imgUrl, bussinessName, style }: OfferProps) => {
    const classes = useStyles();

    return (
        <div style={style} className={classes.root}>
            <img className={classes.img} src={imgUrl} />
            <span className={classes.details}>
                <div style={{ display: "flex", alignItems: "center", textAlign: "start" }}>
                <StoreIcon style={{fontSize: "16"}}/>
                <Typography className={classes.text} style={{ fontSize: "12px", fontWeight: 500 }}>{bussinessName}</Typography>
                </div>
                <Typography className={classes.text} style={{ fontSize: "18px", fontWeight: 500 }}>{name}</Typography>
                <Typography className={classes.text}>{details}</Typography>
                {maxOfferReached && currentInfluencerReached &&
                    <OfferProgressBar {...{ maxOfferReached, currentInfluencerReached }} />
                }
            </span>
        </div>
    );
}

export default Offer;