import axios from "axios"
import { deleteCities, initCities } from "./services/cityService"

export const getIsraelCityData = async () => {
  const data = {
    resource_id: "b7cf8f14-64a2-4b33-8d4b-edb286fdbd37",
    limit: 1500, // 1273
  }

  axios
    .get("https://data.gov.il/api/action/datastore_search", { params: data })
    .then(async(response) => {
      const data = response.data
      
      const cities = data.result.records.map((item: any) => {
        return {
          name: toCamelCase(item.שם_ישוב_לועזי),
        }
      })

      await deleteCities()
      await initCities(cities)
    })
    .catch((error) => {
      console.error(error)
    })
}

const toCamelCase = (str: string): string => {
    str = str.trim()
    const words = str.split(' ');
    const camelCaseWords = words.map((word) => {
      return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
    });
    return camelCaseWords.join(' ');
  }