import React, { useEffect } from "react";
import Login from "./pages/login/login";
import Navbar from "./component/Navbar";
import {  RouteComponentProps } from "@reach/router";
import { makeStyles } from "@material-ui/core";
import { GoogleOAuthProvider } from '@react-oauth/google';
import { FacebookProvider } from 'react-facebook';
import { GQLClient } from "./ApolloClient";
import { ApolloProvider } from "@apollo/client";
import { SearchProvider } from "./pages/context/searchContext";
import { LoggedUserProvider } from "./pages/context/loggedUser";
import StartApp from "./StartApp";

const useStyles = makeStyles((theme) => ({
  root: {
    height: "100vh",
    width: "100vw",
    overflow: "hidden",
    position: "relative"
  }
}));

const App = (props: RouteComponentProps) => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <ApolloProvider client={GQLClient}>
        <GoogleOAuthProvider clientId="356390795695-nus9d90a8lmvmpbbdr73j6m5ioo6h47d.apps.googleusercontent.com">
          <FacebookProvider appId="179029848355384">
            <SearchProvider>
              <LoggedUserProvider>
                <StartApp></StartApp>
              </LoggedUserProvider>
            </SearchProvider>
          </FacebookProvider>
        </GoogleOAuthProvider >
      </ApolloProvider>

    </div >
  )
};

export default App;
