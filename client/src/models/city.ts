export interface City {
  id?: string
  name: string
}

export const cityMapper = (data: any): City => {
  const city: City = {
    id: data.id,
    name: data.name,
  }

  return city
}
