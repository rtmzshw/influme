import { Gender, genderMapper } from "./gender"
import { Business, businessMapper } from "./business"
import { City, cityMapper } from "./city"
import { Influencer, influencerMapper } from "./influencer"
import Offer from "../component/Offer";

export interface Offer {
    id: string;
    business: Business;
    name: string;
    influencer: Influencer[];
    maxOfferReach: number;
    businessPartDetails: string;
    influencerPartDetails: string;
    minAge: number;
    maxAge: number;
    gender: Gender[];
    topAudienceCities: City[];
    imgUrl: string;
  }
  
  export const offerMapper =  (data : any) : Offer => {
    const offer: Offer = {
        id: data.id,
        name: data.name,
        influencer: data.offer_influencer_offers?.map((x : any) => influencerMapper(x.influencer_offer_influencer)),
        businessPartDetails: data.businessPartDetails,
        influencerPartDetails: data.influencerPartDetails,
        maxAge: data.maxAge,
        maxOfferReach: data.maxOfferReach,
        minAge: data.minAge,
        business: businessMapper(data.offer_business),
        topAudienceCities: data.offer_offer_top_cities?.map((x : any) => cityMapper(x.offer_top_cities_city_type)),
        imgUrl: data.imgUrl,
        gender: data.offer_offer_genders?.map((x :any) => genderMapper(x.offer_gender_gender_type)),
    };

    return offer;
  }