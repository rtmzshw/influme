import { navigate } from "@reach/router"
import { Offer, offerMapper } from "./offer";

export enum StatusType {
    DONE = 'Done',
    NOT_AVAILABLE = 'Not Available',
}

export interface IAlert {
    imgUrl: string,
    actionName: string,
    name: string
    action?: any
}

export const alertMapper = (data: any): IAlert => {
    return {
        imgUrl: data.offer.imgUrl,
        actionName: "New Relevant Offer For You",
        name: data.offer.name,
        action: () => navigate(`/app/offer/${data.offer.id}`)
    };
}