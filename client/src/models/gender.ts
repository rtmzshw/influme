export interface Gender {
  id: string
  name: string
}

export const genderMapper = (data: any): Gender => {
  const gender: Gender = {
    id: data.id,
    name: data.name,
  }

  return gender
}
