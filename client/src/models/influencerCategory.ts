export interface InfluencerCategory {
    id: number
    name: string
}