export interface User {
  id?: string
  email?: string
  password?: string
  type?: "User" | "Business"
  phoneNumber?: string
}

export const userMapper =  (data : any) : User => {
  const user: User = {
    id: data?.id,
    email: data?.email,
    password: data?.password,
    type: data?.type,
    phoneNumber: data?.phone_number
  };

  return user;
}