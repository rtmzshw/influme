import { City } from "./city"
import { InfluencerCategory } from "./influencerCategory"
import { User, userMapper } from "./user"

export interface Influencer {
  id?: string
  fullName: string
  user?: User
  instagramHandleName?: string
  categoryName?: string
  profilePic?: string
  instagramProfileLink?: string
  bio?: string
  followersCount?: number
  audience?: Audience
}

export interface Audience {
  influencerId?: string
  minFollowersAge: number
  maxFollowersAge: number
  topCities?: InfluencerTopCity[]
  womanPercentage: number
  menPercentage: number
}

export interface InfluencerTopCity {
  influencerId: string
  cityId: string
}

export const influencerMapper =  (data : any) : Influencer => {
    const influencer: Influencer = {
        id: data.id,
        fullName: data.full_name,
        user: userMapper(data?.influencer_user),
        instagramHandleName: data.instagram_handle_name,
        categoryName: data.category_name,
        profilePic: data.profile_pic,
        instagramProfileLink: data.instagram_profile_link,
        bio: data.bio,
        followersCount: data.followers_count,
    };

    return influencer;
}