import { BusinessCategory } from "./businessCategory"
import { User, userMapper } from "./user"

export interface Business {
    id?: string
    name: string
    profilePic?: string
    bio?: string
    instagramHandleName?: string,
    user?: User
    followersCount?: number
    categoryName?: string
}

export const businessMapper =  (data : any) : Business => {
    return {
        id: data.id,
        name: data.name,
        profilePic: data.profile_pic,
        bio: data.bio,
        instagramHandleName: data.instagram_handle_name,
        user: userMapper(data?.business_user),
        followersCount: data.followers_count,
        categoryName: data.category_name
    };
}