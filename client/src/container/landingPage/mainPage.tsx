import React, { useState } from "react"
import { Grid, Theme, Typography } from "@material-ui/core"
import { makeStyles } from "@material-ui/core/styles"
import DotsStepper from "../../component/dotsStepper"
import influme from "../../images/influme.svg"
import BaseStep from "./baseStep"
import { imagesByStep } from "./imagesByStep"
import { textByStep } from "./textByStep"
import { theme } from "../../Theme"
import BasicButton from "../../component/BasicButton"
import { navigate, RouteComponentProps } from "@reach/router"

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    height: "100vh",
    width: "100vh",
  },
  header: {
    color: "white",
    fontWeight: "bold",
  },
  step: {
    height: "260px",
  },
  children: {
    paddingInline: "14px",
  },
}))

const MainPage = (props: RouteComponentProps) => {
  const classes = useStyles()
  const [activeStep, setActiveStep] = useState(0)

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1)
  }

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1)
  }

  return (
    <Grid
      container
      spacing={3}
      style={{
        backgroundColor:
          activeStep === 0 ? theme.palette.primary.main : "transparent",
          height: "110vh"
      }}>
      <Grid style={{padding: '40px'}} container item spacing={3}>
        <BaseStep
          image={imagesByStep[activeStep]}
          children={
            <div className={classes.children}>
              {activeStep === 1 && <img src={influme}></img>}
              <Typography
                variant='h5'
                className={classes.header}
                style={{
                  color:
                    activeStep === 0 ? "white" : theme.palette.primary.main,
                }}>
                {textByStep[activeStep]}
              </Typography>
            </div>
          }></BaseStep>
      </Grid>
      <Grid container item spacing={3}>
        <DotsStepper
          currentStep={activeStep}
          handleBack={handleBack}
          handleNext={handleNext}
        />
      </Grid>
      <Grid style={{margin :'0', height: '10%'}} justifyContent='center' container item spacing={3}>
        <BasicButton
          title={"Login"}
          colorTheme={activeStep === 0 ? "dark" : "light"}
          onClick={() => navigate("/login")}>
          </BasicButton>
        <BasicButton
          title={"Get started"}
          colorTheme={activeStep === 0 ? "light" : "dark"}
          onClick={() => navigate("/register")}>
          </BasicButton>
      </Grid>
    </Grid>
  )
}

export default MainPage
