import butterfly_body from '../../images/butterfly_body.svg';
import superheroes_tr from '../../images/superheroes_tr.svg';
import loading_data_tr from '../../images/loading_data_tr.svg';
import perfect_code_tr from '../../images/perfect_code_tr.svg';

export const imagesByStep = [
    butterfly_body,
    superheroes_tr,
    loading_data_tr,
    perfect_code_tr
]