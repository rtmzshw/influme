import React from "react";
import { Grid, Theme } from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles"

const useStyles = makeStyles((theme : Theme) => ({
    container: {
      height: '420px'
    },
    image: {
        paddingTop: '20px'
    }
}));

type ButterflyProps = {
    image: string;
    children: JSX.Element
};

const BaseStep = ({ image, children }: ButterflyProps) => {
    const classes = useStyles();

    return (
        <div className={classes.container}>
            <Grid container justifyContent="center">
            <img className={classes.image} src={image} alt="Logo" />
            </Grid>
            {children}
        </div>
        )
};

export default BaseStep;