import { createContext } from "react";
import { Offer } from "../../../models/offer";
import { Exposure } from "../../../services/offersService";

export interface OfferContextType {
  offers: Offer[];
  setOffers: (offers: Offer[]) => void;
  exposure: Exposure;
  setExposure: (exposure: Exposure) => void;
}

export const OfferContext = createContext<OfferContextType>({
  offers: [],
  setOffers: () => {},
  exposure: {comments:0,likes:0,views:0},
  setExposure: () => {},
});