import React, { useState } from 'react';
import { OfferContext, OfferContextType } from './offerContext';
import { Offer } from '../../../models/offer';
import { Exposure } from '../../../services/offersService';

interface OfferProviderProps {
  children: React.ReactNode;
}

export const OfferProvider: React.FC<OfferProviderProps> = ({ children }) => {
  const [offers, setOffers] = useState<Offer[]>([]);
  const [exposure, setExposure] = useState<Exposure>({comments:0,likes:0,views:0});

  const offerContextValue: OfferContextType = {
    offers,
    setOffers,
    exposure,
    setExposure
  };

  return (
    <OfferContext.Provider value={offerContextValue}>
      {children}
    </OfferContext.Provider>
  );
};
