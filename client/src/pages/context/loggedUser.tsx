// SearchContext.tsx
import React, { createContext, useState, useContext, Dispatch, SetStateAction } from 'react';
import { Influencer } from '../../models/influencer';
import { Business } from '../../models/business';
import { IAlert } from '../../models/alert';

interface LoggedUserContextProps {
  user: any;
  setUser: Dispatch<SetStateAction<any | null>>;
  userType: String;
  setUserType:  Dispatch<SetStateAction<String>>;
  reloadUser: boolean;
  setReloadUser:  Dispatch<SetStateAction<boolean>>;
  reloadNotifactions: boolean;
  setReloadNotifactions:  Dispatch<SetStateAction<boolean>>;
  alerts: IAlert[] | null;
  setAlerts: any;
}

export const LoggedUserContext = createContext<LoggedUserContextProps>({
  user: null,
  setUser: () => { },
  userType: "",
  setUserType: () => { },
  reloadUser: false,
  setReloadUser: () => {},
  reloadNotifactions: false,
  setReloadNotifactions: () => {},
  alerts: [],
  setAlerts: () => {},
})

export const useLoggedUserContext = () => useContext(LoggedUserContext);

export const LoggedUserProvider: React.FC = ({ children }) => {
  const [user, setUser] = useState<any>(null);
  const [userType, setUserType] = useState<String>("");
  const [reloadUser, setReloadUser] = useState<boolean>(false);
  const [reloadNotifactions, setReloadNotifactions] = useState<boolean>(false);
  const [alerts, setAlerts] = useState<IAlert[]>([]);

  const contextValue: LoggedUserContextProps = {
    user,
    setUser,
    userType,
    setUserType,
    reloadUser,
    setReloadUser,
    reloadNotifactions,
    setReloadNotifactions,
    alerts,
    setAlerts
  };

  return (
    <LoggedUserContext.Provider value={contextValue}>
      {children}
    </LoggedUserContext.Provider>
  );
};
