// SearchContext.tsx
import React, { createContext, useState, useContext } from 'react';

interface SearchContextProps {
  searchText: string;
  onSearchChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const SearchContext = createContext<SearchContextProps>({
  searchText: '',
  onSearchChange: () => {},
});

export const useSearchContext = () => useContext(SearchContext);

export const SearchProvider: React.FC = ({ children }) => {
  const [searchText, setSearchText] = useState('');

  const handleSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchText(event.target.value);
  };

  const contextValue: SearchContextProps = {
    searchText,
    onSearchChange: handleSearchChange,
  };

  return (
    <SearchContext.Provider value={contextValue}>
      {children}
    </SearchContext.Provider>
  );
};
