import { makeStyles } from "@material-ui/core";
import { RouteComponentProps } from "@reach/router";
import React from "react";
import SmallUpperVector from "../component/Background/SmallUpperVector";
import { createPicComponent } from "../component/InfoCard";
import WorkHistoryItem, { WorkHistoryItemProps } from "../component/WorkHistoryItem";

interface WorkHistoryPageProps extends RouteComponentProps { }

const useStyles = makeStyles((theme) => ({
    root: {
        height: "100%",
        width: "100%",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        marginTop: "15px"
    },
    history: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        width: "100%",
        height: "80%",
        justifyContent: "flex-start",
        overflow: "auto"
    }
}));

const HISTORY: WorkHistoryItemProps[] = [
    {
        _id: "Salon",
        category: "Beauty",
        profileLocation: "Tel Aviv",
        profileName: "Salon",
        imgsrc: "https://www.marketplace.org/wp-content/uploads/2022/07/smallbusiness-1.jpg?fit=2800%2C1870"
    },
    {
        _id: "Uma",
        category: "Food",
        profileLocation: "Zichron Yaakon",
        profileName: "Uma",
        imgsrc: "https://www.marketplace.org/wp-content/uploads/2022/07/smallbusiness-1.jpg?fit=2800%2C1870"
    },
    {
        _id: "Tiulim",
        category: "Travel",
        profileLocation: "Ashdod",
        profileName: "Tiulim",
        imgsrc: "https://www.marketplace.org/wp-content/uploads/2022/07/smallbusiness-1.jpg?fit=2800%2C1870"
    }
]

const WorkHistoryPage = (props: WorkHistoryPageProps) => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <SmallUpperVector />
            {createPicComponent(80)}
            <div style={{ height: "50px" }} />
            <div className={classes.history}>
                {HISTORY.map(historyItem => <WorkHistoryItem {...historyItem} />)}
            </div>
        </div>
    );
}

export default WorkHistoryPage;