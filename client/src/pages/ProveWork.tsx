import { makeStyles } from "@material-ui/core";
import { RouteComponentProps, navigate } from "@reach/router";
import React, { useContext, useState } from "react";
import Navbar from "../component/Navbar";
import { GQLClient } from "../ApolloClient";
import { gql } from "@apollo/client";
import BasicButton from "../component/BasicButton";
import { useLoggedUserContext } from "./context/loggedUser";
import {
  getInfluencerRelevantOffers,
  getInfluencersAmountThatImplementedOffer,
  updateNewRelevantOfferStatus,
  updateOfferStatusToNotAvailable,
} from "../services/influencerRelevantOfferService";
import { getMaxOfferReachById } from "../queries/offers.queries";
import { OfferStatus } from "../consts/offerStatus";
import { updateOfferStatusById } from "../services/offersService";
import BasicTextField from "../component/BasicTextField";
import { OfferContext } from "./context/offer/offerContext";
import { offerMapper } from "../models/offer";

interface ProveWorkPageProps extends RouteComponentProps {}

const useStyles = makeStyles((theme) => ({
  text: { fontWeight: "bold", fontSize: 24 },
  uploadButton: {
    height: "55vh",
    width: "100%",
    border: "#6B4EFF dashed",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  resultImage: {
    height: "55vh",
    width: "100%",
  },
  center: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  stats: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: "15px",
  },
}));

const ProveWork = (props: ProveWorkPageProps & { offerId?: string }) => {
  const classes = useStyles();
  const { offers, setOffers } = useContext(OfferContext);
  const [imageBase64, setImageBase64] = useState<string | undefined>(undefined);
  const [exposureData, setExposureData] = useState<{
    views?: number;
    likes?: number;
    comments?: number;
  }>({});
  const { user } = useLoggedUserContext();
  const handleImageChange = (event: any) => {
    const file = event.target.files[0];
    const reader = new FileReader();

    reader.onloadend = () => {
      const base64String = reader.result;
      setImageBase64(base64String as string);
    };

    if (file) {
      reader.readAsDataURL(file);
    }
  };

  const upload = async () => {
    const res = await GQLClient.mutate({
      mutation: gql`
            mutation MyQuery {
                update_influencer_offer_by_pk(pk_columns: {influencer_id: "${user?.id}", offer_id: "${props.offerId}"}, _set: {approveImage: "${imageBase64}", status: "wating-for-approval", views: "${exposureData.views}", likes: "${exposureData.likes}", comments: "${exposureData.comments}"}) {
                  influencer_id
                }
              }
              `,
    });
    await handleOfferImplementation(props.offerId as string, user?.id);
    await navigate("/app/home", { replace: true });
  };

  const handleOfferImplementation = async (
    offerId: string,
    influencer_id: any
  ) => {
    await updateNewRelevantOfferStatus(
      influencer_id,
      offerId,
      OfferStatus.DONE,
      new Date()
    );
    const allOffers = (await getInfluencerRelevantOffers(user?.id))
            .data.influencer_relevant_offers.map((x: any) => offerMapper(x.offer));
    setOffers(allOffers);

    const influencersAmount = (
      await getInfluencersAmountThatImplementedOffer(offerId)
    ).data.influencer_relevant_offers.length;
    const maxOfferReach = (await getMaxOfferReachById(offerId)).data.offer[0]
      .maxOfferReach;

    if (influencersAmount === maxOfferReach) {
      updateOfferStatusToNotAvailable(offerId, new Date());
      updateOfferStatusById(offerId, "Not Available");
    }
  };

  return (
    <div>
      <Navbar></Navbar>
      <div style={{ padding: "0px 16px" }}>
        <div style={{ height: "12px" }}></div>
        <div className={classes.text}>Upload Screenshot</div>
        <div style={{ height: "12px" }}></div>
        <div style={{ fontWeight: "bold", fontSize: 15 }}>
          prove what you did !
        </div>
        <div style={{ height: "12px" }}></div>

        <div className={classes.stats}>
          <BasicTextField
            style={{ width: "30%" }}
            label={"views"}
            type="number"
            value={exposureData.views}
            onChange={(e) =>
              setExposureData({
                ...exposureData,
                views: Number(e.target.value),
              })
            }
          />
          <BasicTextField
            style={{ width: "30%" }}
            label={"likes"}
            type="number"
            value={exposureData.likes}
            onChange={(e) =>
              setExposureData({
                ...exposureData,
                likes: Number(e.target.value),
              })
            }
          />
          <BasicTextField
            style={{ width: "30%" }}
            label={"comments"}
            type="number"
            value={exposureData.comments}
            onChange={(e) =>
              setExposureData({
                ...exposureData,
                comments: Number(e.target.value),
              })
            }
          />
        </div>

        <input
          type="file"
          accept="image/*"
          onChange={handleImageChange}
          style={{ display: "none" }}
          id="file"
          name="file"
        />
        {imageBase64 ? (
          <div>
            <img
              src={imageBase64}
              alt="Uploaded"
              className={classes.resultImage}
            />
            <div style={{ height: "12px" }}></div>
            <div className={classes.center}>
              <label
                htmlFor="file"
                style={{ flex: 1, fontWeight: "bold", textAlign: "center" }}
              >
                <div>Retake</div>
              </label>
              <div style={{ width: "12px" }}></div>
              <BasicButton
                title="Upload"
                colorTheme="gradient"
                style={{ flex: 2, fontWeight: "bold" }}
                onClick={upload}
              ></BasicButton>
            </div>
          </div>
        ) : (
          <label htmlFor="file">
            <div className={classes.uploadButton}>
              <div
                className={classes.center}
                style={{ flexDirection: "column" }}
              >
                <img src={require("../assets/upload.png")} height="83px"></img>
                <div style={{ height: "12px" }}></div>
                <div style={{ fontWeight: "semi-bold", fontSize: 15 }}>
                  Choose from gallery
                </div>
              </div>
            </div>
          </label>
        )}
      </div>
    </div>
  );
};
export default ProveWork;
