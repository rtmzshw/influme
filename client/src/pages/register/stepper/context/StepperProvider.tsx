import React, { useState } from "react";
import {
  StepperContext,
  StepperContextProps,
  getSteps,
} from "./StepperContext";

interface StepperProviderProps {
  children: React.ReactNode;
}

export const StepperProvider: React.FC<StepperProviderProps> = ({
  children,
}) => {
  const [activeStep, setActiveStep] = useState(0);
  const [accountType, setAccountType] = useState(0);

  const handleNext = () => {
    setActiveStep(activeStep + 1);
  };

  const handleBack = () => {
    setActiveStep(activeStep - 1);
  };

  const contextValue: StepperContextProps = {
    activeStep,
    handleNext,
    handleBack,
    steps: getSteps(accountType),
    setActiveStep,
    accountType,
    setAccountType,
  };

  return (
    <StepperContext.Provider value={contextValue}>
      {children}
    </StepperContext.Provider>
  );
};
