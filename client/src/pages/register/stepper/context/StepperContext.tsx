import React, { createContext } from "react"
import SignUp from "../../SignUp"
import SignUpIntro from "../../SignUpIntro"
import InfluencerForm from "../../InfluencerForm"
import InstaRegister from "../../InstaRegister"
import { AccountType } from "../../../../consts/accountType"
import BusinessForm from "../../BusinessForm"

export const getSteps = (accountType?: AccountType) => {
  if (!accountType) return ["Sign Up", "Who are you?", "What's your instagram?", "More info"] 
return accountType === AccountType.Influencer ? 
     ["Sign Up", "Who are you?","What's your instagram?" , "Influencer Register"] :
     ["Sign Up", "Who are you?","What's your instagram?" , "Small Buisness Register"] 
  }
  
  export const getStepContent = (step: number, accountType?: AccountType) => {
    switch (step) {
      case 0:
        return { id: 0, title: "Sign Up", component: <SignUp />, showNext: false }
      case 1:
        return {
          id: 1,
          title: "Who are you?",
          component: <SignUpIntro />,
          showNext: false,
        }
      case 2:
        return {
          id: 2,
          title: "What's your instagram?",
          component: <InstaRegister />,
          showNext: true,
        }
      case 3:
        return accountType === AccountType.Influencer ? 
        {id: 3, title: "Influencer Register", component: <InfluencerForm /> }:
         {id: 4,title: "Small Buisness Register", component: <BusinessForm /> } 

      default:
        return { title: "Unknown step" }
    }
  }

  
export interface StepperContextProps {
  activeStep: number
  handleNext: () => void
  handleBack: () => void,
  setActiveStep: (activeStep: number) => void
  steps: string[]
  accountType: AccountType
  setAccountType: (accountType: AccountType) => void,
}


export const StepperContext = createContext<StepperContextProps | undefined>(
  undefined
)
