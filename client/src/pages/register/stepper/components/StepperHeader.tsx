import Step from "@material-ui/core/Step"
import StepLabel from "@material-ui/core/StepLabel"
import Stepper from "@material-ui/core/Stepper"
import React, { useContext } from "react"
import { QontoConnector } from "./QontoConnector"
import QontoStepIcon from "./QontoStepIcon"
import { StepperContext } from "../context/StepperContext"

const StepperHeader = () => {
  const stepperContext = useContext(StepperContext)

  return stepperContext ? (
    <>
      <Stepper
        alternativeLabel
        activeStep={stepperContext.activeStep}
        connector={<QontoConnector />}
        style={{ backgroundColor: "transparent" }}>
        {stepperContext.steps.map((label) => (
          <Step key={label}>
            <StepLabel StepIconComponent={QontoStepIcon}>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
    </>
  ) : (
    <></>
  )
}

export default StepperHeader
