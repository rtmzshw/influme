import { Grid, makeStyles } from "@material-ui/core"
import Typography from "@material-ui/core/Typography"
import React, { useContext } from "react"
import { StepperContext, getStepContent } from "../context/StepperContext"
import BasicIconButton from "../../../../component/BasicIconButton"
import ArrowBackIcon from "@material-ui/icons/ArrowBack"
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';

const useStyles = makeStyles((theme) => ({
  btnRight: {
    marginTop: "auto",
    display: "flex",
    justifyContent: "flex-end",
  },
}))
const StepperContent = () => {
  const classes = useStyles()
  const stepperContext = useContext(StepperContext)

  return stepperContext ? (
    <div style={{ height: "70vh", overflow: "auto" }}>
      {stepperContext.activeStep === stepperContext.steps.length ? (
        <Grid item xs={12}>
          <Typography>All steps completed - you&apos;re finished</Typography>
        </Grid>
      ) : (
        <>
          <Grid item xs={12}>
            {
              getStepContent(
                stepperContext.activeStep,
                stepperContext.accountType
              ).component
            }
          </Grid>

<Grid container>
          {(stepperContext.activeStep !== 0 && stepperContext.activeStep !== 1) && (
            <Grid item xs={6}>
              <BasicIconButton
                colorTheme='light'
                onClick={stepperContext.handleBack}>
                <ArrowBackIcon />
              </BasicIconButton>
            </Grid>
          )}
          {getStepContent(stepperContext.activeStep, stepperContext.accountType)
            .showNext && (
            <Grid item xs={6}>
              <div className={classes.btnRight}>
            <BasicIconButton
              colorTheme='light'
              onClick={stepperContext.handleNext}>
              <ArrowForwardIcon />
            </BasicIconButton>
            </div>
          </Grid>
          )}
          </Grid>
        </>
      )}
    </div>
  ) : (
    <></>
  )
}

export default StepperContent
