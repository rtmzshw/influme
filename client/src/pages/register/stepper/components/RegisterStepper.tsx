import { Grid } from "@material-ui/core"
import Typography from "@material-ui/core/Typography"
import { Theme, createStyles, makeStyles } from "@material-ui/core/styles"
import { RouteComponentProps } from "@reach/router"
import React from "react"
import StepperContent from "./StepperContent"
import StepperHeader from "./StepperHeader"
import { StepperProvider } from "../context/StepperProvider"
import SmallUpperVector from "../../../../component/Background/SmallUpperVector"
import BottomVector from "../../../../component/Background/BottomVector"
import { RegisterProvider } from "../../context/RegisterProvider"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    container: {
      padding: "5%",
    },
    button: {
      marginRight: theme.spacing(1),
    },
    instructions: {
      marginTop: theme.spacing(1),
      marginBottom: theme.spacing(1),
    },
    img: {
      margin: theme.spacing(4),
    },
    title: {
      fontSize: "33px",
      fontWeight: 700,
    },
    subTitle: {
      color: "rgba(107, 78, 255, 0.2)",
      fontSize: "14px",
      fontWeight: 700,
    },
  })
)

const RegisterStepper = (props: RouteComponentProps) => {
  const classes = useStyles()

  return (
    <StepperProvider>
      <div className={classes.root}>
        <SmallUpperVector />
        <BottomVector />
        <Grid container spacing={1} className={classes.container}>
          <Grid item xs={12}>
            <Typography color='primary' className={classes.title}>
              Sign Up
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography className={classes.subTitle}>Let's meet!</Typography>
          </Grid>

          <RegisterProvider>
            <Grid item>
              <StepperHeader />
            </Grid>

            <StepperContent />
          </RegisterProvider>
        </Grid>
      </div>
    </StepperProvider>
  )
}

export default RegisterStepper
