import { CircularProgress, Grid } from "@material-ui/core"
import Button from "@material-ui/core/Button"
import { makeStyles } from "@material-ui/core/styles"
import React, { useContext, useState } from "react"
import BasicTextField from "../../component/BasicTextField"
import FileInputButton from "../../component/FileInputButton"
import { RegisterContext } from "./context/RegisterContext"
import { saveNewBusiness } from "../../queries/register.queries"
import { navigate } from "@reach/router"

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: "5%",
  },
  form: {
    display: "flex",
    flexDirection: "column",
  },
  button: {
    margin: theme.spacing(2),
  },
  img: {
    margin: theme.spacing(4),
  },
  profileImg: {
    width: "100px",
    height: "100px",
    borderRadius: "50%",
    overflow: "hidden",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  progress: {
    position: "fixed",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(255, 255, 255, 0.7)",
    zIndex: 9999,
  }
}))

const BusinessForm = () => {
  const classes = useStyles()

  const [isLoading, setIsLoading] = useState(false);
  const [selectedFile, setSelectedFile] = useState<string | null>(null)
  const registerContext = useContext(RegisterContext)

  const [formData, setFormData] = useState({
    businessName: registerContext?.instgarmScraperData?.full_name,
    businessBio: registerContext?.instgarmScraperData?.biography,
    businessCategory: registerContext?.instgarmScraperData?.category_name,
    phoneNumber: ""
  })

  const [formError, setFormError] = useState(false)
  const [errorMsg, setErrorMsg] = useState<string>()

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }))
  }

  const handleSubmit = async () => {
    if (!formData.businessName || !formData.businessBio) {
      setFormError(true)
      return
    }

    if (!selectedFile) {
      setFormError(true)
      setErrorMsg("please choose profile photo")
      return
    }

    setFormError(false)
    const business = {
      name: formData.businessName,
      bio: formData.businessBio,
      categoryName: formData.businessCategory,
      followersCount: registerContext?.instgarmScraperData?.followersCount,
      instagramHandleName: registerContext?.instgarmScraperData?.handleName,
      profilePic: selectedFile,
    }

    setIsLoading(true);
    const userId = await registerContext?.setBusinessData(business)
    setIsLoading(false);
    navigate(`/app/home`)
  }

  const handleFileChange = (file: string | null) => {
    setSelectedFile(file)
  }

  return (
    <div className={classes.root}>
      <form className={classes.form}>
        <Grid
          container
          direction="row"
          justifyContent="center"
          alignItems="center"
          spacing={2}
        >
          <FileInputButton
            label="Choose Profile Photo"
            onChange={handleFileChange}
          />

          <Grid item xs={12}>
            <BasicTextField
              name="businessName"
              label="Business Name"
              value={formData.businessName}
              onChange={handleChange}
              error={formError && !formData.businessName}
              helperText={
                formError && !formData.businessName ? "Field is required" : ""
              }
            />
          </Grid>
          <Grid item xs={12}>
            <BasicTextField
              type="number"
              name="phoneNumber"
              label="Phone Number"
              value={formData.phoneNumber}
              onChange={(e) => {
                handleChange(e as any)
                registerContext?.setUserData({ ...registerContext?.userData, phoneNumber: e.target.value })
              }}
            />
          </Grid>
          <Grid item xs={12}>
            <BasicTextField
              name="businessBio"
              label="Business Bio"
              value={formData.businessBio}
              onChange={handleChange}
              minRows={3}
              multiline
              error={formError && !formData.businessBio}
              helperText={
                formError && !formData.businessBio ? "Field is required" : ""
              }
            />
          </Grid>
          <Grid item xs={12}>
            <BasicTextField
              name="businessCategory"
              label="Business Category"
              value={formData.businessCategory}
              onChange={handleChange}
              disabled={true}
            />
          </Grid>
          {formError && (
            <Grid item xs={12}>
              <span style={{ color: "red" }}>{errorMsg}</span>
            </Grid>
          )}
          {isLoading && (
            <div className={classes.progress}>
                <CircularProgress />
            </div>
          )}     
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            onClick={handleSubmit}
          >
            Submit
          </Button>
        </Grid>
      </form>
    </div>
  )
}

export default BusinessForm
