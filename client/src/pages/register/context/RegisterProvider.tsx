import React, { useState } from "react"
import { Business } from "../../../models/business"
import { Influencer } from "../../../models/influencer"
import { User } from "../../../models/user"
import { RegisterContextData, RegisterContext } from "./RegisterContext"
import { InstgramScaperData } from "../../../InstagramScraper"
import {
  saveNewBusiness,
  saveNewInfluencer,
  saveNewInfluencerAudience,
  saveNewInfluencerCities,
  saveNewUser,
  saveNewUserProvider,
} from "../../../queries/register.queries"
import { findRelevantOffers } from "../../../services/matchInfluencerToOffer"
import { useLoggedUserContext } from "../../context/loggedUser"

export const RegisterProvider: React.FC = ({ children }) => {
  const [userData, setUserData] = useState<User | undefined>(undefined)
  const [influencerData, setInfluencerData] = useState<Influencer | undefined>(
    undefined
  )
  const [businessData, setBusinessData] = useState<Business | undefined>(
    undefined
  )
  const [instgarmScraperData, setInstgarmScraperData] = useState<
    InstgramScaperData | undefined
  >(undefined)
  const { setReloadUser, reloadUser } = useLoggedUserContext()

  const saveUserDB = async (user: User) => {
    try {
      let response
      if (user.id) {
        response = await saveNewUserProvider(user)
      } else {
        response = await saveNewUser(user)
      }
      setUserData({ ...userData, id: response.data.insert_user_one.id })
      setLoggedUserCookie({
        id: response.data.insert_user_one.id,
        type: response.data.insert_user_one.type,
      })

      return response.data.insert_user_one.id
    } catch (error) {
      console.error("Failed to save user to db:", error)
    }
  }

  const saveBusinessDB = async (business: Business) => {
    try {
      return saveNewBusiness(business)
    } catch (error) {
      console.error("Failed to save business to db:", error)
    }
  }

  const saveInfluencerDB = async (influencer: Influencer) => {
    try {
      const response = await saveNewInfluencer(influencer)
      const influencerId = response.data.insert_influencer_one.id

      if (influencer.audience) {
        await saveNewInfluencerAudience({
          ...influencer.audience,
          influencerId,
        })
      }
      let topCities = influencer.audience?.topCities
      if (topCities) {
        topCities = topCities.map((city) => {
          return {
            influencerId,
            cityId: city.cityId,
          }
        })
        await saveNewInfluencerCities(topCities)

        const cities = topCities.map((city) => city.cityId)

        return findRelevantOffers({
          influencerId,
          minAge: influencer.audience?.minFollowersAge || 0,
          maxAge: influencer.audience?.maxFollowersAge || 100,
          womanPercentage: influencer.audience?.womanPercentage || 0,
          topAudienceCities: cities,
        })
      }
    } catch (error) {
      console.error("Failed to save influencer to db:", error)
    }
  }

  const saveBusinessData = async (business: Business) => {
    if (userData) {
      const userId = await saveUserDB({ ...userData, type: "Business" })
      business = { ...business, user: { id: userId } }
      setBusinessData(business)
      await saveBusinessDB(business)
      setReloadUser(!reloadUser)
      return userId
    }
  }

  const saveInfluencerData = async (influencer: Influencer) => {
    if (userData) {
      const userId = await saveUserDB({ ...userData, type: "User" })
      influencer = { ...influencer, user: { id: userId } }
      setInfluencerData(influencer)
      await saveInfluencerDB(influencer)
      setReloadUser(!reloadUser)
      return userId
    }
  }

  const setLoggedUserCookie = (userCookie: {
    type: "User" | "Business"
    id: string
  }) => {
    const cookieData = JSON.stringify(userCookie)
    document.cookie = `loggedUser=${cookieData}`
  }

  const contextValue: RegisterContextData = {
    userData,
    influencerData,
    businessData,
    instgarmScraperData,
    setInstgarmScraperData,
    setUserData,
    setInfluencerData: saveInfluencerData,
    setBusinessData: saveBusinessData,
  }

  return (
    <RegisterContext.Provider value={contextValue}>
      {children}
    </RegisterContext.Provider>
  )
}
