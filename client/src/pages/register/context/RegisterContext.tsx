import React from "react"
import { Business } from "../../../models/business"
import { Influencer } from "../../../models/influencer"
import { User } from "../../../models/user"
import { InstgramScaperData } from "../../../InstagramScraper"

export interface RegisterContextData {
  userData?: User
  influencerData?: Influencer
  businessData?: Business
  instgarmScraperData?: InstgramScaperData
  setInstgarmScraperData: (instgramScraperData: InstgramScaperData) => void
  setUserData: (userData: User) => void
  setInfluencerData: (influencerData: Influencer) => Promise<void>
  setBusinessData: (businessData: Business) => Promise<void>
}

export const RegisterContext = React.createContext<
  RegisterContextData | undefined
>(undefined)
