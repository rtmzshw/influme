import { CircularProgress, Grid } from "@material-ui/core"
import Button from "@material-ui/core/Button"
import { makeStyles } from "@material-ui/core/styles"
import { default as React, useContext, useEffect, useState } from "react"
import AgeSlider from "../../component/AgeSlider"
import BasicTextField from "../../component/BasicTextField"
import FileInputButton from "../../component/FileInputButton"
import { MultipleSelect } from "../../component/MultipleSelect"
import { getAllCities } from "../../services/cityService"
import { RegisterContext } from "./context/RegisterContext"
import MultipleAutoComplete from "../../component/MultipleAutoComplete"
import { navigate } from "@reach/router"

const categories = ["Clothing", "Fashion", "Art"]

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: "5%",
  },
  form: {
    display: "flex",
    flexDirection: "column",
  },
  button: {
    margin: theme.spacing(2),
  },
  img: {
    margin: theme.spacing(4),
  },
  profileImg: {
    width: "100px",
    height: "100px",
    borderRadius: "50%",
    overflow: "hidden",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  progress: {
    position: "fixed",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(255, 255, 255, 0.7)",
    zIndex: 9999,
  }
}))

const InfluencerForm = () => {
  const classes = useStyles()

  const [isLoading, setIsLoading] = useState(false);
  const [selectedFile, setSelectedFile] = useState<string | null>(null)
  const registerContext = useContext(RegisterContext)
  const [cities, setCities] = useState<{ id: string; name: string }[]>([])
  const [errorMsg, setErrorMsg] = useState<string>()
  const [formError, setFormError] = useState(false)
  const [formData, setFormData] = useState({
    fullName: registerContext?.instgarmScraperData?.full_name,
    phoneNumber: '',
    minFollowersAge: 20,
    maxFollowersAge: 80,
    womanPercentage: 0,
    manPercentage: 0,
    topFiveCities: [""],
    profilePic: "",
  })

  useEffect(() => {
    const fetchCities = async () => {
      try {
        const response = await getAllCities()
        setCities(response.data.city_type)
      } catch (error) {
        console.error("Failed to fetch cities:", error)
      }
    }

    fetchCities()
  }, [])

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }))
    if (name === "womanPercentage") {
      setFormData((prevData) => ({
        ...prevData,
        manPercentage: 100 - Number(value),
      }))
    }
    if (name === "manPercentage") {
      setFormData((prevData) => ({
        ...prevData,
        womanPercentage: 100 - Number(value),
      }))
    }
    
  }

  const handleSubmit = async () => {
    setFormError(false)
    if (
      !formData.fullName ||
      !formData.manPercentage ||
      !formData.womanPercentage ||
      !formData.phoneNumber ||
      formData.topFiveCities.includes("")
    ) {
      setFormError(true)
      setErrorMsg("Please fill in all the fields.")
      return
    }
    if (
      Number(formData.manPercentage) + Number(formData.womanPercentage) !==
      100
    ) {
      setFormError(true)
      setErrorMsg("total percentage needs to add up to 100")
      return
    }
    if (!selectedFile) {
      setFormError(true)
      setErrorMsg("please choose profile photo")
      return
    }

    setFormError(false)
    const topInfluencerCities = formData.topFiveCities.map((cityId) => {
      return {
        cityId,
        influencerId: "0",
      }
    })

    const audienceData = {
      minFollowersAge: formData.minFollowersAge,
      maxFollowersAge: formData.maxFollowersAge,
      womanPercentage: formData.womanPercentage,
      menPercentage: formData.manPercentage,
      topCities: topInfluencerCities,
    }

    console.log(formData)
    const influencerData = {
      fullName: registerContext?.instgarmScraperData?.full_name || "",
      user: { id: registerContext?.userData?.id },
      instagramHandleName: registerContext?.instgarmScraperData?.handleName,
      categoryName: registerContext?.instgarmScraperData?.category_name,
      profilePic: selectedFile,
      phoneNumber: formData.phoneNumber,
      instagramProfileLink:
        registerContext?.instgarmScraperData?.instagramProfileLink,
      bio: registerContext?.instgarmScraperData?.biography,
      followersCount: registerContext?.instgarmScraperData?.followersCount,
      audience: audienceData,
    }

    setIsLoading(true);
   const userId =  await registerContext?.setInfluencerData(influencerData)
    setIsLoading(false);
    navigate(`/app/home`)
  }

  const handleFileChange = (file: string | null) => {
    setSelectedFile(file)
  }

  const handleSliderChange = (min: number, max: number) => {
    setFormData({ ...formData, minFollowersAge: min, maxFollowersAge: max })
  }

  const handleSelectionChange = (selectedOptions: string[]) => {
    setFormData({ ...formData, topFiveCities: selectedOptions })
  }

  return (
    <div className={classes.root}>
      <form className={classes.form}>
        <Grid
          container
          direction="row"
          justifyContent="center"
          alignItems="center"
          spacing={2}
        >
          <FileInputButton
            label="Choose Profile Photo"
            onChange={handleFileChange}
          />

          <Grid item xs={12}>
            <BasicTextField
              name="fullName"
              label="Full Name"
              value={formData.fullName}
              onChange={handleChange}
              disabled={true}
            />
          </Grid>
          <Grid item xs={12}>
            <BasicTextField
              type="number"
              name="phoneNumber"
              label="Phone Number"
              value={formData.phoneNumber}
              onChange={(e) => {
                handleChange(e as any)
                registerContext?.setUserData({ ...registerContext?.userData, phoneNumber: e.target.value })
              }}
            />
          </Grid>
          <Grid item xs={12}>
            <AgeSlider
              minAge={formData.minFollowersAge}
              maxAge={formData.maxFollowersAge}
              onSliderChange={handleSliderChange}
            />
          </Grid>
          <Grid item xs={6}>
            <BasicTextField
            type="number"
              suffix="%"
              name="manPercentage"
              label="man Percentage"
              value={formData.manPercentage}
              onChange={handleChange}
            />
          </Grid>
          <Grid item xs={6}>
            <BasicTextField
            type="number"
              suffix="%"
              name="womanPercentage"
              label="woman Percentage"
              value={formData.womanPercentage}
              onChange={handleChange}
            />
          </Grid>
          <Grid item xs={12}>
            <MultipleAutoComplete
              items={cities}
              label="Top Audience Cities"
              onChange={handleSelectionChange}
            />
          </Grid>
          {formError && (
            <Grid item xs={12}>
              <span style={{ color: "red" }}>{errorMsg}</span>
            </Grid>
          )}
          {isLoading && (
            <div className={classes.progress}>
              <CircularProgress />
            </div>
          )}
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            onClick={handleSubmit}
          >
            Submit
          </Button>
        </Grid>
      </form>
    </div>
  )
}

export default InfluencerForm
