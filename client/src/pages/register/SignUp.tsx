import { Grid, makeStyles } from "@material-ui/core"
import ArrowForwardIcon from "@material-ui/icons/ArrowForward"
import { RouteComponentProps } from "@reach/router"
import React, { useContext, useState } from "react"
import BottomVector from "../../component/Background/BottomVector"
import SmallUpperVector from "../../component/Background/SmallUpperVector"
import BasicTextField from "../../component/BasicTextField"
import BasicIconButton from "../../component/BasicIconButton"
import { RegisterContext } from "../register/context/RegisterContext"
import { StepperContext } from "../register/stepper/context/StepperContext"
import ProviderSignIn from "../login/ProviderSignIn"
import BasicButton from "../../component/BasicButton"

interface SignUpProps extends RouteComponentProps {}

const useStyles = makeStyles((theme) => ({
  buttonContainer: {
    display: "flex",
    justifyContent: "center",
  },
}))

interface UserData {
  email: string
  password: string
  confirmPassword: string // New property for confirm password
}

const SignUp = (props: SignUpProps) => {
  const classes = useStyles()
  const registerContext = useContext(RegisterContext)
  const [userData, setUserData] = useState<UserData>({
    email: registerContext?.userData?.email || "",
    password: registerContext?.userData?.password || "",
    confirmPassword: registerContext?.userData?.password || "",
  })
  const [emailError, setEmailError] = useState("")
  const [passwordError, setPasswordError] = useState("")

  const stepperContext = useContext(StepperContext)

  const onNext = () => {
    setEmailError("")
    setPasswordError("")
    let passwordValid = true
    let emailValid = true
    if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(userData.email)) {
      setEmailError("Invalid email")
      emailValid = false
    }
    if (!userData.password) {
      setPasswordError("Please enter password")
      passwordValid = false
    }
    if (userData.password !== userData.confirmPassword) {
      setPasswordError("Passwords do not match")
      passwordValid = false
    }
    if (passwordValid && emailValid) {
      const user = {
        email: userData.email,
        password: userData.password,
      }
      registerContext?.setUserData(user)
      stepperContext?.handleNext()
    }
  }

  const handleSetUserId = ({userId, email}: {email?: string, userId: string}) => {
    const user = {
      id: userId,
      email
    }
    registerContext?.setUserData(user)
    stepperContext?.handleNext()
  }

  return (
    <Grid container spacing={2}>
      <SmallUpperVector />

      <Grid item xs={12}>
        <BasicTextField
          label={"Email"}
          placeholder="example@gmail.com"
          value={userData.email} // Add the value prop
          onChange={(e) => setUserData({ ...userData, email: e.target.value })}
          error={
            !!emailError // Use emailError instead of userData.email for error checking
          }
          helperText={
            emailError // Use emailError for displaying the error message
          }
        />
      </Grid>

      <Grid item xs={12}>
        <BasicTextField
          label="Password"
          type="password"
          value={userData.password}
          onChange={(e) =>
            setUserData({ ...userData, password: e.target.value })
          }
          error={!!passwordError} // Set error prop to boolean value
          helperText={passwordError} // Display error message as helper text
        />
      </Grid>
      <Grid item xs={12}>
        <BasicTextField
          label="Confirm Password"
          type="password"
          value={userData.confirmPassword}
          onChange={(e) =>
            setUserData({ ...userData, confirmPassword: e.target.value })
          }
          error={!!passwordError} // Set error prop to boolean value
          helperText={passwordError} // Display error message as helper text
        />
      </Grid>
      <Grid item xs={12}>
        <div className={classes.buttonContainer}>
          <BasicButton title="Approve" colorTheme="gradient" style={{ fontWeight: "bold", width:"100%" }} onClick={onNext}></BasicButton>          
        </div>
      </Grid>
      <BottomVector />
      <Grid item xs={12}>
        <ProviderSignIn setUserId={handleSetUserId} />
      </Grid>
    </Grid>
  )
}

export default SignUp
