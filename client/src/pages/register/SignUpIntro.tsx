import { makeStyles } from "@material-ui/core";
import { RouteComponentProps } from "@reach/router";
import React, { useContext, useState } from "react";
import BasicButton from "../../component/BasicButton";
import { AccountType } from "../../consts/accountType";
import { RegisterContext } from "./context/RegisterContext";
import { StepperContext } from "./stepper/context/StepperContext";

interface SignUpProps extends RouteComponentProps {}

const useStyles = makeStyles((theme) => ({
  root: {
    height: "40vh",
  },
  center: {
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    margin: "auto",
  },
  business: {
    position: "absolute",
    top: 0,
    right: 0,
  },
  influencer: {
    position: "absolute",
    bottom: 0,
    left: 0,
  },
  title: {
    fontSize: "33px",
    fontWeight: 700,
  },
  subTitle: {
    color: "rgba(107, 78, 255, 0.2)",
    fontSize: "14px",
    fontWeight: 700,
  },
  fadeIn: {
    opacity: 0,
    transition: "opacity 0.4s ease-in-out",
  },
}));

const SignUpIntro = (props: SignUpProps) => {
  const classes = useStyles();
  const stepperContext = useContext(StepperContext);
  const registerContext = useContext(RegisterContext);
  const [fadeIn, setFadeIn] = useState(false); // State to control the animation

  const onBtnClick = (accountType: AccountType) => {
    setFadeIn(true);
    stepperContext?.setAccountType(accountType);
    setTimeout(() => {
      stepperContext?.handleNext(); // Call the handleNext function after the delay
    }, 400);
  };

  return (
    <div className={classes.root}>
      <div
        className={`${classes.center} ${fadeIn ? classes.fadeIn : ""}`}
        style={{ height: "40vh" }}
      >
        <svg
          className={classes.center}
          width="315"
          height="276"
          viewBox="0 0 315 276"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M313.678 123.123C314.06 156.513 316.424 186.465 300.597 199.982C284.621 213.317 250.454 210.217 226.57 219.072C202.537 227.745 188.947 248.242 164.899 261.785C141.16 275.38 107.121 281.891 80.8974 268.972C54.6734 256.053 36.5718 223.757 21.3002 190.366C6.17739 157.158 -5.95587 122.726 3.89436 98.2961C13.5959 73.6835 45.1212 59.203 69.2459 43.4722C93.2218 27.559 109.935 10.8904 131.105 4.24928C152.434 -2.52206 178.21 1.04664 209.729 4.48523C241.396 8.10614 278.807 11.5969 296.707 31.9126C314.607 52.2282 313.146 89.551 313.678 123.123Z"
            fill="#6B4EFF"
            fill-opacity="0.21"
          />
        </svg>
        <img
          alt="signUpInto"
          className={classes.center}
          height={"213px"}
          src={require("../../assets/signup.png")}
        />
        <BasicButton
          style={{
            position: "absolute",
            bottom: 0,
            left: 10,
          }}
          title="I’m an influencer"
          colorTheme="light"
          onClick={() => {
            onBtnClick(AccountType.Influencer);
          }}
        />
        <BasicButton
          style={{
            position: "absolute",
            top: 0,
            right: 10,
          }}
          title="I’m a small business"
          colorTheme="light"
          onClick={() => {
            onBtnClick(AccountType.Buisness);
          }}
        />
      </div>
    </div>
  );
};

export default SignUpIntro;
