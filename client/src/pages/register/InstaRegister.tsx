import {
  CircularProgress,
  Grid,
  LinearProgress,
  makeStyles,
  Typography,
} from "@material-ui/core";
import { RouteComponentProps } from "@reach/router";
import React, { useContext, useEffect, useState } from "react";
import BottomVector from "../../component/Background/BottomVector";
import BasicButton from "../../component/BasicButton";
import BasicTextField from "../../component/BasicTextField";
import {
  getUserInstgramData,
  InstgramScaperData,
} from "../../InstagramScraper";
import DisplayInstgramScaperData from "../../component/DisplayInstgramScaperData";
import { RegisterContext } from "./context/RegisterContext";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: "5%",
  },
  title: {
    fontSize: "33px",
    fontWeight: 700,
  },
  subTitle: {
    color: "rgba(107, 78, 255, 0.2)",
    fontSize: "14px",
    fontWeight: 700,
  },
  center: {
    width: "95%",
    margin: "auto",
    padding: "12px 0px",
  },
}));

const FIELDS = {
  instagramName: "Instgram Handle Name",
};

const InstaRegister = () => {
  const classes = useStyles();
  const registerContext = useContext(RegisterContext);
  const [inputData, setInputData] = useState({
    instagramName: registerContext?.instgarmScraperData?.handleName || "",
  });
  const [loading, setLoading] = useState(false);
  const [progress, setProgress] = useState(0);
  const [instgarmScraperData, setInstgarmScraperData] =
    useState<InstgramScaperData>();

  useEffect(() => {
    if (registerContext?.instgarmScraperData) {
      setInstgarmScraperData(registerContext?.instgarmScraperData);
    }
  }, []);

  const fetchData = async () => {
    setInterval(
      () => setProgress((progress) => Math.min(90, progress + 10)),
      600
    );
    setLoading(true);
    const userData = await getUserInstgramData(inputData.instagramName);
    if (userData) {
      setInstgarmScraperData(userData);
      registerContext?.setInstgarmScraperData({
        handleName: inputData.instagramName,
        instagramProfileLink: `https://www.instagram.com/${inputData.instagramName}/`,
        ...userData,
      });
    }
    setProgress(100);
    setLoading(false);
  };

  const handleInputValueChange = (event: any) => {
    const { name, value } = event.target;
    setInputData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  return (
    <div className={classes.root}>
      <Grid container spacing={2}>
        {Object.keys(FIELDS).map((key) => (
          <Grid item xs={12} key={key}>
            <BasicTextField
              prefix="@"
              name={key}
              value={inputData.instagramName}
              label={FIELDS[key as keyof typeof FIELDS]}
              onChange={handleInputValueChange}
            />
          </Grid>
        ))}
        {loading && (
          <div className={classes.center}>
            <LinearProgress variant="determinate" value={progress} />
          </div>
        )}
        <Grid item xs={12}>
          <BasicButton
            title="fetch intagram data"
            colorTheme="dark"
            key="signup"
            onClick={fetchData}
          />
        </Grid>
        <Grid item xs={12}></Grid>
      </Grid>
      <BottomVector />
      {instgarmScraperData && (
        <DisplayInstgramScaperData data={instgarmScraperData} />
      )}
    </div>
  );
};

export default InstaRegister;
