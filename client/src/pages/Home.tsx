import { CircularProgress, Fab, makeStyles } from "@material-ui/core"
import ChatBubbleIcon from '@material-ui/icons/ChatBubble'
import ThumbUpAltIcon from '@material-ui/icons/ThumbUpAlt'
import VisibilityIcon from '@material-ui/icons/Visibility'
import { RouteComponentProps, useNavigate } from "@reach/router"
import { useContext, useEffect, useState } from "react"
import Offer from "../component/Offer"
import TrendCard from "../component/TrendCard"
import { offerMapper } from "../models/offer"
import { getInfluencerRelevantOffers } from "../services/influencerRelevantOfferService"
import { Exposure, getBusinessExposureFromOffers, getBusinessOffers } from "../services/offersService"
import { useLoggedUserContext } from "./context/loggedUser"
import { OfferContext } from "./context/offer/offerContext"
import { useSearchContext } from "./context/searchContext"
import AddIcon from '@material-ui/icons/Add';
import React from "react";
import BottomVector from "../component/Background/BottomVector"
import UpperVector from "../component/Background/UpperVector"

const useStyles = makeStyles((theme) => ({
  root: {
    height: "100%",
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    overflow: "auto",
  },
  addButton: {
    position: "absolute",
    bottom: theme.spacing(1),
    left: theme.spacing(1),
    zIndex: 1,
    "&:hover": {
      transform: "scale(1.1)",
    },
    "& .MuiSvgIcon-root": {
      fontSize: "2.8rem",
    },
  },
  offer: {
    width: "85%"
  },
  trends: {
    display: "flex",
    flexWrap: "wrap",
    alignItems: "center",
    justifyContent: 'space-around',
    width: "90%",
  },
  progress: {
    position: "fixed",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(255, 255, 255, 0.7)",
    zIndex: 9999,
  }
}))

interface HomePageProps extends RouteComponentProps { }

const HomePage = (props: HomePageProps) => {
  const classes = useStyles()
  const navigate = useNavigate()
  const { offers, setOffers, exposure, setExposure } = useContext(OfferContext);
  const { user, userType } = useLoggedUserContext()
  const { searchText } = useSearchContext();
  const [isLoading, setIsLoading] = useState(true);

  const getAllOffers = async () => {
    let allOffers;
    if (!offers || !offers.length) {
      if (user?.id) {
        if (userType === "User") {
          setIsLoading(true);
          allOffers = (await getInfluencerRelevantOffers(user?.id))
            .data.influencer_relevant_offers.map((x: any) => offerMapper(x.offer));
          setIsLoading(false);
        } else {
          setIsLoading(true);
          allOffers = await getBusinessOffers(user?.id)
          const businessExposure = await getBusinessExposureFromOffers(allOffers);
          setExposure(businessExposure);
          setIsLoading(false);
        }
      }

      //@ts-ignore
      setOffers(allOffers)
    } else {
      setIsLoading(false)
    }
  }

  useEffect(() => { getAllOffers() }, [user]);

  const filteredOffers = offers?.filter(({ name }) =>
    name.toLowerCase().includes(searchText.toLowerCase()))

  return (
    <div>
      {userType === "Business" ?
        <div
          style={{
            width: "100%",
            height: '15vh',
            display: "flex",
            alignItems: "center",
            flexDirection: "column",
            color: '#3d3761'
          }}
        >
          <div style={{ fontSize: '22px', fontWeight: 500, marginBottom: '16px', marginTop: '4px' }}>
            Social Exposure
          </div>
          <div className={classes.trends}>
            <TrendCard count={exposure.views} trend="Views" icon={<VisibilityIcon style={{ fontSize: "24px" }} />} />
            <TrendCard count={exposure.comments} trend="Comments" icon={<ChatBubbleIcon style={{ fontSize: "24px" }} />} />
            <TrendCard count={exposure.likes} trend="Likes" icon={<ThumbUpAltIcon style={{ fontSize: "24px" }} />} />
          </div>
        </div> :
        <>
        </>
      }
      <UpperVector />
      <BottomVector />
      {isLoading && (
        <div className={classes.progress}>
          <CircularProgress />
        </div>
      )}
      <div className={classes.root}>
      <div style={{ fontSize: '22px', fontWeight: 500, color: '#3d3761' }}>
          Available Offers
      </div>
        {filteredOffers?.map(
          ({ id, name, influencerPartDetails, maxOfferReach, influencer, businessPartDetails, imgUrl, business }) => (
            <div className={classes.offer} onClick={() => navigate(`/app/offer/${id}`)}>
              <Offer
                name={name}
                key={id}
                details={userType === "Business" ? influencerPartDetails : businessPartDetails}
                maxOfferReached={maxOfferReach}
                currentInfluencerReached={influencer}
                imgUrl={imgUrl}
                bussinessName={business.name}
              />
            </div>
          )
        )}
        {userType === "Business" && (
          <div className={classes.addButton}>
            <Fab
              style={{ margin: "12px" }}
              color="primary"
              onClick={() => navigate("/app/newOffer")}>
              <AddIcon fontSize="small" />
            </Fab>
          </div>
        )}
      </div >
    </div>
  )
}

export default HomePage
