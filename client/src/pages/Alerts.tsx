import { CircularProgress, Hidden, makeStyles } from "@material-ui/core";
import { RouteComponentProps, useLocation } from "@reach/router";
import React, { useEffect, useState } from "react";
import BottomVector from "../component/Background/BottomVector";
import UpperVector from "../component/Background/UpperVector";
import { IAlert } from "../models/alert";
import { getInfluencerAlerts } from "../services/alertService";
import Alert from "../component/Alert";
import SmallUpperVector from "../component/Background/SmallUpperVector";
import { gql } from "@apollo/client";
import { useLoggedUserContext } from "./context/loggedUser";

interface AlertsPageProps extends RouteComponentProps { }

const useStyles = makeStyles((theme) => ({
  root: {
    height: "100%",
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginTop: "15px",
  },
  alertItem: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    width: "100%",
    height: "80%",
    justifyContent: "flex-start",
    overflow: "auto",
  },
  noAlerts: {
    position: "relative",
    height: "50vh",
    width: "75%",
    display: "flex",
    textAlign: "center",
    flexDirection: "column",
    justifyContent: "center",
    fontSize: "40px",
    padding: "10px",
    alignItems: "center",
    margin: "20px",
    fontWeight: 700,
    color: "rgb(140 119 247 / 82%)",
  },
  loader: {
    marginTop: "150px",
  },
  title: {
    fontSize: "25px",
    marginTop: "10px",
    fontWeight: 800,
    color: "rgb(140 119 247 / 82%)",
  },
}));

const GET_WATING_FOR_APPROVAL = (businessId: number) => gql`
query MyQuery {
  offer(where: {business_id: {_eq: "${businessId}"}}) {
    offer_influencer_offers(where: {status: {_eq: "wating-for-approval"}}) {
      status
      offer_id
      influencer_id
    }
  }
}
`;

const AlertsPage = (props: AlertsPageProps) => {
  const classes = useStyles();
  const location: any = useLocation()
  const {alerts: notifications} = useLoggedUserContext()
  return (
    <div className={classes.root}>
      <SmallUpperVector />
      {notifications && notifications.length !== 0 ? notifications.map(n => <div style={{ margin: "8px 0px", width: '90%' }}><Alert alert={n}></Alert></div>) : <div className={classes.noAlerts}> There is no alerts for you.. </div>}
      <BottomVector />
    </div>
  );
};

export default AlertsPage;
