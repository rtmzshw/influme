import { makeStyles } from "@material-ui/core";
import { RouteComponentProps, navigate } from "@reach/router";
import React from "react";
import Navbar from "../component/Navbar";
import { GQLClient } from "../ApolloClient";
import { gql, useQuery } from "@apollo/client";
import BasicButton from "../component/BasicButton";
import { CircularProgress } from "@material-ui/core";
import clsx from "clsx";
import { useLoggedUserContext } from "./context/loggedUser";

interface ApproveWorkPageProps extends RouteComponentProps {}

const useStyles = makeStyles((theme) => ({
  text: { fontWeight: "bold", fontSize: 24 },
  uploadButton: {
    height: "60vh",
    width: "100%",
    border: "#6B4EFF dashed",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  resultImage: {
    height: "60vh",
    width: "100%",
  },
  center: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
}));
const GET_PROVE_IMAGE = (offerId: string, influencerId: string) => gql`
query MyQuery {
    influencer_offer_by_pk(influencer_id: "${influencerId}", offer_id: "${offerId}") {
      approveImage
    }
  }
`;

const ApproveWork = (
  props: ApproveWorkPageProps & { offerId?: string; influencerId?: string }
) => {
  const classes = useStyles();
  const { loading, data } = useQuery(
    GET_PROVE_IMAGE(props.offerId as string, props.influencerId as string)
  );
  const { setReloadNotifactions, reloadNotifactions } =
  useLoggedUserContext()

  const approve = async () => {
    await GQLClient.mutate({
      mutation: gql`
            mutation MyQuery {
                update_influencer_offer_by_pk(pk_columns: {influencer_id: "${props.influencerId}", offer_id: "${props.offerId}"}, _set: {status: "approved"}) {
                    offer_id
                }
              }
            `,
    });
    setReloadNotifactions(!reloadNotifactions)
    await navigate("/app/home", { replace: true });
  };

  return (
    <div>
      <div style={{ padding: "0px 16px" }}>
        <div style={{ height: "12px" }}></div>
        <div className={classes.text}>Approve work</div>
        <div style={{ height: "12px" }}></div>
        <div style={{ fontWeight: "bold", fontSize: 15 }}>
          Make sure you got what you asked !
        </div>
        <div style={{ height: "12px" }}></div>
        {loading ? (
          <div className={clsx(classes.center, classes.resultImage)}>
            <CircularProgress />
          </div>
        ) : (
          <div>
            <img
              src={data.influencer_offer_by_pk.approveImage}
              alt="Uploaded"
              className={classes.resultImage}
            />
            <div style={{ height: "12px" }}></div>
            <div className={classes.center}>
              <label
                htmlFor="file"
                style={{ flex: 1, fontWeight: "bold", textAlign: "center" }}
              >
                <div>Decline</div>
              </label>
              <div style={{ width: "12px" }}></div>
              <BasicButton
                title="Approve"
                colorTheme="gradient"
                style={{ flex: 2, fontWeight: "bold" }}
                onClick={approve}
              ></BasicButton>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default ApproveWork;
