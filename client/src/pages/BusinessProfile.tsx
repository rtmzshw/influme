import { CircularProgress, IconButton, makeStyles } from "@material-ui/core";
import MailOutlineOutlinedIcon from "@material-ui/icons/MailOutlineOutlined";
import PhoneOutlinedIcon from "@material-ui/icons/PhoneOutlined";
import { RouteComponentProps, navigate } from "@reach/router";
import React, { useEffect, useState } from "react";
import BottomVector from "../component/Background/BottomVector";
import UpperVector from "../component/Background/UpperVector";
import ImageList from "../component/ImageList";
import InfoCard from "../component/InfoCard";
import ProfileData from "../component/ProfileData";
import { businessMapper } from "../models/business";
import { getBusinessByUserId } from "../queries/business.queries";
import { Influencer } from "../models/influencer";
import { Business } from "../models/business";
import { getBusinessInfluencersHistory } from "../queries/influencerOffers.queries";
import CopyDialog from "../component/CopyDialog";

interface BusinessProfilePageProps extends RouteComponentProps {
  userId?: string;
}

const useStyles = makeStyles((theme) => ({
  root: {
    height: "100%",
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  images: {
    overflow: "auto",
    width: "95%",
    display: "flex",
    alignSelf: "end",
    alignItems: "center",
  },
  actions: {
    display: "flex",
    alignItems: "center",
    width: "90%",
    justifyContent: "space-around",
  },
  progress: {
    position: "fixed",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(255, 255, 255, 0.7)",
    zIndex: 9999,
  },
}));

const BusinessProfilePage = (props: BusinessProfilePageProps) => {
    const classes = useStyles();
    const { userId } = props;
    const [business, setBusiness] = useState<Business>({} as any);
    const [openPhoneDialog, setOpenPhoneDialog] = useState(false);
    const [openMailDailog, setOpenMailDialog] = useState(false);
    const [influencersHistory, setInfluencersHistory] = useState<{ all: Influencer[], uniq: Influencer[] }>({ uniq: [], all: [] });
    const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const getProfile = async () => {
      setIsLoading(true);
      if (userId) {
        const res = await getBusinessByUserId(userId);
        if (res.data.business[0]){
          const data = businessMapper(res.data.business[0]);
          const influencers = await getBusinessInfluencersHistory(
            data.id?.toString() ?? ""
          );
          setBusiness(data);
          setInfluencersHistory(influencers);
        } else { 
          navigate('/app/notFound')
        }
  
      }
      setIsLoading(false);
    };

    getProfile();
  }, []);

  const navigateToInfluencerPage = (influencerId) => {
    navigate(`/app/profile/User/${influencerId}`)
  }

  const { bio, categoryName, instagramHandleName, profilePic, name } = business;

  return (
    <div className={classes.root}>
      <UpperVector />
      {isLoading && (
        <div className={classes.progress}>
          <CircularProgress />
        </div>
      )}
      <InfoCard
        {...{
          details: bio ?? "",
          title: name ?? "",
          pic: profilePic,
          instaName: instagramHandleName ?? "",
          instaLink: `https://www.instagram.com/${instagramHandleName}`,
        }}
      >
        {
          <div className={classes.actions}>
            <IconButton onClick={() => setOpenPhoneDialog(!openMailDailog)}>
              <PhoneOutlinedIcon color="primary" />
            </IconButton>
            <IconButton onClick={() => setOpenMailDialog(!openMailDailog)}>
              <MailOutlineOutlinedIcon color="primary" />
            </IconButton>
          </div>
        }
      </InfoCard>
      <CopyDialog
        text={
          openMailDailog
            ? business.user?.email ?? ""
            : business.user?.phoneNumber ?? ""
        }
        open={openMailDailog || openPhoneDialog}
        setOpen={openMailDailog ? setOpenMailDialog : setOpenPhoneDialog}
      ></CopyDialog>
      <div style={{ height: "40px" }} />
      <ProfileData
        {...{
          businessAmount: influencersHistory.all.length,
          category: categoryName ?? "",
          profileLocation: "",
          profileName: name ?? "",
        }}
      />
      <div style={{ height: "20px" }} />
      {influencersHistory.uniq.length > 0 && (
        <ImageList
          style={{ height: "13%" }}
          images={influencersHistory.uniq.map(({ profilePic, fullName, user }) => ({
            src: profilePic ?? "",
            name: fullName,
            userId: user?.id ? user.id.toString() : "",
          }))}
          onClick={navigateToInfluencerPage}
          imageStyle={{
            height: "50px",
            width: "50px",
            borderRadius: "50%",
            padding: "5px",
          }}
          title={`Influencers worked with ${name}`}
        />
      )}
      <BottomVector />
    </div>
  );
};

export default BusinessProfilePage;
