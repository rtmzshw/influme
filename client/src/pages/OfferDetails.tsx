import { RouteComponentProps, useNavigate } from "@reach/router";
import React, { useContext, useEffect, useState } from "react";
import {
  Chip,
  Grid,
  makeStyles,
  Paper,
  Typography,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  CircularProgress,
} from "@material-ui/core";
import { Offer } from "../models/offer";
import OfferProgressBar from "../component/OfferProgressBar";
import RoomIcon from "@material-ui/icons/Room";
import AgeSlider from "../component/AgeSlider";
import StoreIcon from '@material-ui/icons/Storefront'
import FaceIcon from "@material-ui/icons/Face";
import UpperVector from "../component/Background/UpperVector";
import BottomVector from "../component/Background/BottomVector";
import { OfferContext } from "./context/offer/offerContext";
import BasicIconButton from "../component/BasicIconButton";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import DeleteIcon from "@material-ui/icons/Delete";
import BasicButton from "../component/BasicButton";
import { gql } from "@apollo/client";
import { GQLClient } from "../ApolloClient";
import { useLoggedUserContext } from "./context/loggedUser";
import { deleteOfferById } from "../services/offersService";
import SmallUpperVector from "../component/Background/SmallUpperVector";
import TextBubbles from "../component/TextBubble";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: "5%",
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    height: "20vh",
  },
  rectangle: {
    height: "15vh",
    backgroundColor: "white",
    boxShadow: "0px 10px 20px rgba(0, 95, 183, 0.3)",
    borderRadius: "5px",
    display: "flex",
    alignItems: "center",
    marginTop: "-18px"
  },
  img: {
    height: "100%",
    width: "110px",
    minWidth: "90px",
    borderRadius: "2px"
  },
  title: {
    fontSize: "22px",
    fontWeight: "bold",
  },
  subtitle: {
    fontSize: "20px",
  },
  text: {
    alignSelf: "flex-start",
    paddingTop: "5px",
    height: "90%",
    width: "65%",
    position: "relative",
    paddingLeft: "5%",
    overflow: "auto"
  },
  backButton: {
    padding: "0",
  },
  flex: {
    display: "flex",
    justifyContent: "space-between",
    padding: "15px",
  },
  progress: {
    position: "fixed",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(255, 255, 255, 0.7)",
    zIndex: 9999,
  }
}));

interface OfferDetailsPageProps extends RouteComponentProps {
  id?: string;
}

const OfferDetails = (props: OfferDetailsPageProps) => {
  const classes = useStyles();
  const navigate = useNavigate();
  const { id } = props;
  const { user, userType } = useLoggedUserContext();
  const { offers, setOffers } = useContext(OfferContext);
  const offersContext = useContext(OfferContext);
  const [offer, setOffer] = useState<Offer>();
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (offersContext.offers.length) {
      const currOffer = offersContext.offers.find((offer) => offer?.id === id);
      setOffer(currOffer);
    }
  }, []);

  const deleteOffer = () => {
    setIsDialogOpen(!isDialogOpen);
  };

  const confirmDeleteOffer = async () => {
    await deleteOfferById(id);
    setOffers(offers.filter((x) => x.id !== id));
    setIsDialogOpen(!isDialogOpen);
    navigate("/app/home");
  };

  const takeOffer = async () => {
    setIsLoading(true);

    await GQLClient.mutate({
      mutation: gql`
      mutation MyMutation {
        insert_influencer_offer_one(object: {influencer_id: "${user?.id}", offer_id: "${id}", status: ""}) {
          offer_id
        }
      }
      `,
      // if already exists it will throw
    }).catch((e) => e);

    setIsLoading(false);
    navigate(`/proveWork/${id}`);
  };

  return (
    <>
      <div className={classes.flex}>
        {isLoading && (
            <div className={classes.progress}>
                <CircularProgress />
            </div>
        )}  
        <BasicIconButton
          colorTheme="light"
          onClick={() => navigate("/app/home")}
          classes={{ root: classes.backButton }}
        >
          <ArrowBackIcon />
        </BasicIconButton>
        {userType === "Business" && user.id === offer?.business.id &&
          < BasicIconButton
            colorTheme="light"
            onClick={deleteOffer}
            classes={{ root: classes.backButton }}
          >
            <DeleteIcon />
          </BasicIconButton>
        }
      </div >
      {offer && (
        <div className={classes.root}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <div className={classes.rectangle} >
                <img
                  className={classes.img}
                  alt={offer?.imgUrl}
                  src={offer?.imgUrl}
                />
                <span className={classes.text}>
                <div style={{ display: "flex", alignItems: "center", textAlign: "start" }}>
                <StoreIcon style={{fontSize: "20"}}/>
                  <Typography className={classes.title}>
                    {offer?.business.name}
                  </Typography>
                  </div>
                  <Typography className={classes.subtitle}>
                    {offer?.name}
                  </Typography>
                </span>
              </div>
            </Grid>
            <Grid item xs={12}>
              <OfferProgressBar
                maxOfferReached={offer.maxOfferReach}
                currentInfluencerReached={offer.influencer}
              ></OfferProgressBar>
            </Grid>

            <Grid item xs={12}>
              <TextBubbles businessPart={offer.businessPartDetails} influencerPart={offer.influencerPartDetails} />
            </Grid>
            {offer?.gender?.map(({ name }) => (
              <Grid item>
                <Chip label={name} icon={<FaceIcon />} variant="outlined" />
              </Grid>
            ))}

            <Grid item xs={12}>
              <AgeSlider
                minAge={offer?.minAge}
                maxAge={offer?.maxAge}
                disabled={true}
              ></AgeSlider>
            </Grid>

            {offer?.topAudienceCities?.map((city) => (
              <Grid item>
                <Chip
                  label={city.name}
                  icon={<RoomIcon />}
                  variant="outlined"
                />
              </Grid>
            ))}
          </Grid>
          {userType === "User" && (
            <>
              <div style={{ height: "12px" }}></div>
              <BasicButton
                title="Take Offer"
                colorTheme="gradient"
                style={{ width: "100%", fontWeight: "bold" }}
                onClick={takeOffer}
              ></BasicButton>
            </>
          )}
        </div>
      )
      }
      <SmallUpperVector />
      <BottomVector />
      <Dialog
        open={isDialogOpen}
        onClose={() => setIsDialogOpen(!isDialogOpen)}
      >
        <DialogContent>
          <Typography>Are you sure you want to delete this offer?</Typography>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() => setIsDialogOpen(!isDialogOpen)}
            color="primary"
          >
            Cancel
          </Button>
          <Button onClick={confirmDeleteOffer} color="primary">
            Delete
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default OfferDetails;
