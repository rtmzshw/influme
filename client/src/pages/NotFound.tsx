import { makeStyles } from "@material-ui/core";
import { RouteComponentProps, navigate } from "@reach/router";
import React from "react";
import BottomVector from "../component/Background/BottomVector";
import UpperVector from "../component/Background/UpperVector";
import influme from "../images/influme.svg";

interface NotFoundPageProps extends RouteComponentProps {}

const useStyles = makeStyles((theme) => ({
  root: {
    height: "100%",
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginTop: "15px",
  },
  alertItem: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    width: "100%",
    height: "80%",
    justifyContent: "flex-start",
    overflow: "auto",
  },
  wrongPageTitle: {
    position: "relative",
    height: "50vh",
    width: "75%",
    display: "flex",
    textAlign: "center",
    flexDirection: "column",
    justifyContent: "center",
    fontSize: "40px",
    padding: "10px",
    alignItems: "center",
    margin: "20px",
    fontWeight: 700,
    color: "rgb(140 119 247 / 82%)",
  },
  title: {
    fontSize: "20px",
  },
}));

const NotFoundPage = (props: NotFoundPageProps) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <UpperVector />
      <img src={influme}></img>
      <div className={classes.wrongPageTitle}>
        Sorry, you are in the wrong page{" "}
      </div>
      <div className={classes.title} onClick={(e) => navigate("/app/home")}>
        To Home Page
      </div>
      <BottomVector />
    </div>
  );
};

export default NotFoundPage;
