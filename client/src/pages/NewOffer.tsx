import React, { useContext, useEffect, useState } from "react";
import { RouteComponentProps } from "@reach/router";
import BasicTextField from "../component/BasicTextField";
import { CircularProgress, makeStyles } from "@material-ui/core";
import AgeSlider from "../component/AgeSlider";
import BasicButton from "../component/BasicButton";
import UpperVector from "../component/Background/UpperVector";
import BottomVector from "../component/Background/BottomVector";
import GenderButtonGroup from "../component/GenderButtonGroup";
import { useNavigate } from "@reach/router";
import { getAllCities } from "../services/cityService";
import FileInputButton from "../component/FileInputButton";
import { Gender } from "../models/gender";
import { getGenders } from "../services/genderService";
import { addOffer, addOfferTopCity, addOfferTopGender, getOfferById } from "../services/offersService";
import MultipleAutoComplete from "../component/MultipleAutoComplete";
import { matchOfferInfluencer } from "../services/matchOfferInfluencer";
import { useLoggedUserContext } from "./context/loggedUser";
import { OfferContext } from "./context/offer/offerContext";

interface NewOfferProps extends RouteComponentProps { }

const useStyles = makeStyles((theme) => ({
    root: {
        padding: "5%",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
    },
    actions: {
        display: "flex",
        alignItems: "center",
        width: "80%",
        justifyContent: "space-around",
    }, 
    overlay: {
        position: "fixed",
        top: 0,
        left: 0,
        width: "100%",
        height: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "rgba(255, 255, 255, 0.7)",
        zIndex: 9999,
      }
}));

const NewOffer = (props: NewOfferProps) => {
    const classes = useStyles();
    const navigate = useNavigate();
    const { offers, setOffers } = useContext(OfferContext);
    const [cities, setCities] = useState<{ id: string; name: string }[]>([]);
    const [genders, setGenders] = useState<Gender[]>([]);
    const [selectedFile, setSelectedFile] = useState<string | null>(null)
    const [offerName, setOfferName] = useState("");
    const [maxOfferReach, setMaxOfferReach] = useState("");
    const [businessPartDetails, setBusinessPartDetails] = useState("");
    const [influencerPartDetails, setInfluencerPartDetails] = useState("");
    const [minAge, setMinAge] = useState(0);
    const [maxAge, setMaxAge] = useState(100);
    const [selectedCities, setSelectedCities] = useState<string[]>([]);
    const [selectedGenders, setSelectedGenders] = useState<string[]>([]);
    const [errorMsg, setErrorMsg] = useState<string>()
    const [isLoading, setIsLoading] = useState(false);
    const { user } = useLoggedUserContext()
    
    useEffect(() => {
        const fetchCities = async () => {
            try {
              const response = await getAllCities();
              setCities(response.data.city_type);
            } catch (error) {
              console.error("Failed to fetch cities:", error);
            }
        };

        const fetchGenders = async () => {
            try {
              const response = await getGenders();
              setGenders(response.data.gender_type);
            } catch (error) {
              console.error("Failed to fetch cities:", error);
            }
        };
      
        fetchCities();
        fetchGenders();
    }, []); 
    
    const handleFileChange = (file: string | null) => {
        setSelectedFile(file)
    }

    const handleSave = async () => {
        if (
            !offerName ||
            !maxOfferReach ||
            !businessPartDetails ||
            !influencerPartDetails ||
            selectedCities.length === 0 ||
            selectedGenders.length === 0
        ) {
            setErrorMsg("Please fill in all the fields.")
            return
        }
        if (!selectedFile) {
            setErrorMsg("Please choose an image")
            return
        }

        try {
            let result;
            setIsLoading(true);
            if(user?.id){
                result = await addOffer(
                    offerName,
                    user?.id?.toString(),
                    parseInt(maxOfferReach),
                    businessPartDetails,
                    influencerPartDetails,
                    minAge,
                    maxAge,
                    selectedFile || ""
                  );
            }

          const offerId = result;

          for (const cityId of selectedCities) {
            await addOfferTopCity(cityId, offerId);
          }

          for (const genderId of selectedGenders) {
            await addOfferTopGender(genderId, offerId);
          }

          const newoffer = await getOfferById(offerId)

          if (!offers || !offers.length) {
            setOffers([newoffer]);
          } else {
            setOffers([...offers, newoffer]);
          }

          await matchOfferInfluencer({
            offerId,
            minAge,
            maxAge,
            genders: selectedGenders,
            topAudienceCities: selectedCities,
          })

          setIsLoading(false);
          navigate('/app/home');
        } catch (error) {
          console.error("Failed to add offer:", error);
          setIsLoading(false);
        }
    };

    const handleAgeChange = (min: number, max: number) => {
        setMinAge(min);
        setMaxAge(max);
    };

    const handleCitiesSelectionChange = (selectedOptions: string[]) => {
        setSelectedCities(selectedOptions)
    };

    const handleGendersSelectionChange = (selectedOptions: string[]) => {
        setSelectedGenders(selectedOptions)
    };

    return (
        <div className={classes.root}>
            <UpperVector/>
            <BottomVector/>
            <span style={{ fontSize: "30px", marginBottom: "20px" }}>New Offer</span>
            <FileInputButton
                label='Upload an Image'
                onChange={handleFileChange}
            />
            <BasicTextField
                label={"Offer Name"}
                value={offerName}
                onChange={(e) => setOfferName(e.target.value)}
            ></BasicTextField>
            <BasicTextField
                label={"Max Offer Reach"}
                type={"number"}
                value={maxOfferReach}
                onChange={(e) => setMaxOfferReach(e.target.value)}
            ></BasicTextField>
            <BasicTextField
            minRows={3}
            multiline
                label={"Business Part Details"}
                value={businessPartDetails}
                onChange={(e) => setBusinessPartDetails(e.target.value)}
            ></BasicTextField>

            <BasicTextField
            minRows={3}
            multiline
                label={"Influencer Part Details"}
                value={influencerPartDetails}
                onChange={(e) => setInfluencerPartDetails(e.target.value)}
            ></BasicTextField>
            <MultipleAutoComplete
                items={cities}
                label={"Top Audience Cities"}
                onChange={handleCitiesSelectionChange}
            />
            <span style={{ marginRight: "195px", marginTop: "15px" }}>Gender:</span>
            <GenderButtonGroup 
                genders={genders}
                onSelectionChange={handleGendersSelectionChange}
            />
            <AgeSlider
                onSliderChange={handleAgeChange}
            />
            <span style={{ color: "red", marginBottom: "15px" }}>{errorMsg}</span>
            {isLoading && (
                <div className={classes.overlay}>
                    <CircularProgress />
                </div>
            )}            
            <div className={classes.actions}>
                <BasicButton
                    colorTheme="gradient"
                    title={"Save"}
                    style={{ width: "45%", fontSize: "13px" }}
                    onClick={handleSave}
                />              
                <BasicButton 
                    colorTheme="gradient" 
                    title={"Cancel"} 
                    style={{ width: "45%", fontSize: "13px" }}
                    onClick={() => navigate('/app/home')}
                />
            </div>
        </div>
    );
}

export default NewOffer;