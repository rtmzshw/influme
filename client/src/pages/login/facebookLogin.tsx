import FaceBookLoginPackage from "@greatsumini/react-facebook-login"
import React from "react"
import facebookSvg from "../../images/facebook.svg"

const FacebookLogin = (props: { setUserId: ({userId, email}: {email?: string, userId: string}) => void }) => {
  return (
    <FaceBookLoginPackage
      appId="179029848355384"
      onSuccess={(response) => {
        console.log("Login Success!", response)
      }}
      onFail={(error) => {
        console.log("Login Failed!", error)
      }}
      onProfileSuccess={(response) => {
        console.log("Get Profile Success!", response)
        props.setUserId({userId: response.id!})
      }}
      render={({ onClick, logout }) => (
        <img
          src={facebookSvg}
          onClick={onClick}
          style={{ width: "50px" }}
          alt="Logo"
        />
      )}
    />
  )
}
export default FacebookLogin
