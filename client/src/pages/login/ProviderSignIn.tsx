import { makeStyles, TextField } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import React, { useState } from "react";
import googlePng from '../../images/google.svg';
import { navigate, RouteComponentProps } from "@reach/router";
import { useGoogleLogin } from "@react-oauth/google";
import { useLogin } from "react-facebook";
import axios from 'axios'
import FacebookLogin from "./facebookLogin";
import { getUserByEmailPassword, getUserById } from "../../userService";
const useStyles = makeStyles((theme) => ({
    pageContainer: {
        padding: "24px",
    },
    loginBtn: {
        "display": "flex"
    },
    signUp: {
        color: theme.palette.primary.main,
        textDecoration: "underline"
    },
    input: {
        width: "100%"
    },
    horizontalLine: {
        borderBottom: `1px ${theme.palette.primary.main} solid`,
        flex: 1,
        height: "1px"
    },
    resetPassword: {
        color: theme.palette.primary.main,
        textAlign: "center"
    },
}));


const ProviderSignIn = (props: RouteComponentProps & { setUserId: ({userId, email}: {email?: string, userId: string}) => void }) => {
    const classes = useStyles();

    const loginViaGoogle = useGoogleLogin({
        onSuccess: async codeResponse => {
            console.log(codeResponse);

            const options = {
                headers: {
                    Authorization: `Bearer ${codeResponse.access_token}`,
                    Accept: 'application/json'
                }
            }
            const { data } = await axios
                .get(`https://www.googleapis.com/oauth2/v3/userinfo?access_token=${codeResponse.access_token}`, options)
            console.log(data);

            props.setUserId({userId: data.sub, email: data.email})

        },
        flow: 'implicit',
        scope: 'openid email profile',
    });

    return (
        <>
         <div style={{ height: "48px" }}></div>
            <div style={{ display: "flex", alignItems: "center" }}>
                <div className={classes.horizontalLine}></div>
                <Button variant="outlined" color="primary" >Or SignIn using</Button>
                <div className={classes.horizontalLine}></div>
            </div>
            <div style={{ height: "48px" }}></div>
            <div style={{ display: "flex", justifyContent: "space-around" }}>
                <img onClick={() => loginViaGoogle()} src={googlePng} style={{ width: "50px" }} alt="Logo" />
                <FacebookLogin setUserId={props.setUserId}></FacebookLogin>
            </div>
        </>
    );
};

export default ProviderSignIn;