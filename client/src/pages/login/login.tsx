import { CircularProgress, makeStyles, TextField } from "@material-ui/core"
import Button from "@material-ui/core/Button"
import { navigate, RouteComponentProps } from "@reach/router"
import { useGoogleLogin } from "@react-oauth/google"
import axios from "axios"
import React, { useState } from "react"
import { getUserByEmailPassword, getUserById } from "../../userService"
import ProviderSignIn from "./ProviderSignIn"
import { useLoggedUserContext } from "../context/loggedUser"

const useStyles = makeStyles((theme) => ({
  pageContainer: {
    padding: "24px",
  },
  loginBtn: {
    display: "flex",
  },
  signUp: {
    color: theme.palette.primary.main,
    textDecoration: "underline",
  },
  input: {
    width: "100%",
  },
  horizontalLine: {
    borderBottom: `1px ${theme.palette.primary.main} solid`,
    flex: 1,
    height: "1px",
  },
  resetPassword: {
    color: theme.palette.primary.main,
    textAlign: "center",
  },
  progress: {
    position: "fixed",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(255, 255, 255, 0.7)",
    zIndex: 9999,
  }
}))

const Login = (props: RouteComponentProps) => {
  const classes = useStyles()
  const [email, setEmail] = useState<string>("")
  const [password, setPassword] = useState<string>("")
  const { setReloadUser, reloadUser } = useLoggedUserContext()
  const [isLoading, setIsLoading] = useState(false);

  const loginViaEmailPassword = async () => {
    try {
      setIsLoading(true);
      const response = await getUserByEmailPassword(email, password)

      const user = response.data.user[0]
      setLoggedUserCookie({ type: user.type, id: user.id })
      setIsLoading(false);
      navigate(`/app/home`)
    } catch (e) {
      setIsLoading(false);
      console.log(e)
    }
  }

  const setLoggedUserCookie = (userCookie: {
    type: "User" | "Business"
    id: string
  }) => {
    const cookieData = JSON.stringify(userCookie);
    document.cookie = `loggedUser=${cookieData}`
    setReloadUser(!reloadUser)
  }

  const isFormValid = () => {
    const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/
    const isValidEmail = regex.test(email)
    return isValidEmail 
  }

  const LoginViaProvider = async (id: string) => {
    try {
      const response = await getUserById(id)
      const user = response.data.user_by_pk
      setLoggedUserCookie({ type: user.type, id: user.id })
      navigate(`/app/home`)
    } catch (e) {
      console.log(e)
    }
  }
  const handleSetUserId = ({userId, email}: {email?: string, userId: string}) => {
    LoginViaProvider(userId)
  }

  return (
    <div className={classes.pageContainer}>
      <div style={{ height: "40px" }}></div>
      <h1>Welcome Back !</h1>
      <div style={{ display: "flex" }}>
        <div>Don't have an account?</div>
        <div style={{ width: "12px" }}></div>
        {/* <Link className={classes.signUp} to={"/signUp"}> */}
        <div className={classes.signUp} onClick={(e) => navigate("/register")}>
          SignUp
        </div>
        {/* </Link> */}
      </div>
      <div style={{ height: "80px" }}></div>
      <TextField
        className={classes.input}
        variant="outlined"
        label="email"
        onChange={(e) => setEmail(e.target.value)}
      />
      <div style={{ height: "24px" }}></div>
      <TextField
        className={classes.input}
        variant="outlined"
        label="password"
        onChange={(e) => setPassword(e.target.value)}
        type="password"
      />
      <div style={{ height: "16px" }}></div>
      {isLoading && (
        <div className={classes.progress}>
            <CircularProgress />
        </div>
      )} 
      {/* <Link className={classes.loginBtn} to={"/"}> */}
      <Button
        disabled={!isFormValid()}
        style={{ width: "100%" }}
        variant="contained"
        color="primary"
        onClick={(e) => loginViaEmailPassword()}
      >
        Login
      </Button>
      {/* </Link> */}
      <div style={{ height: "30px" }}></div>
      <div className={classes.resetPassword}>Forgot your password?</div>
      <div style={{ height: "48px" }}></div>
      <ProviderSignIn setUserId={handleSetUserId}/>
    </div>
  )
}

export default Login
