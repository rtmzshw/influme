import { CircularProgress, IconButton, makeStyles } from "@material-ui/core";
import MailOutlineOutlinedIcon from "@material-ui/icons/MailOutlineOutlined";
import PhoneOutlinedIcon from "@material-ui/icons/PhoneOutlined";
import { RouteComponentProps, navigate } from "@reach/router";
import React, { useEffect, useState } from "react";
import BottomVector from "../component/Background/BottomVector";
import UpperVector from "../component/Background/UpperVector";
import ImageList from "../component/ImageList";
import InfoCard from "../component/InfoCard";
import ProfileData from "../component/ProfileData";
import {
  getInfluencerByUserId,
  getInfluencerTopCities,
} from "../queries/influencer.queries";
import { Influencer, influencerMapper } from "../models/influencer";
import { Business } from "../models/business";
import { getInfluencerBusinessHistory, getInfluencerOnGoingOffersById } from "../queries/influencerOffers.queries";
import CopyDialog from "../component/CopyDialog";
import Offer from "../component/Offer";

interface ProfilePageProps extends RouteComponentProps {
  userId?: string;
}

const useStyles = makeStyles((theme) => ({
  root: {
    height: "100%",
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  images: {
    overflow: "auto",
    width: "95%",
    display: "flex",
    alignSelf: "end",
    alignItems: "center",
  },
  actions: {
    display: "flex",
    alignItems: "center",
    width: "90%",
    justifyContent: "space-around",
  },
  progress: {
    position: "fixed",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(255, 255, 255, 0.7)",
    zIndex: 9999,
  },
  offers: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginTop: "8px",
    overflow: "auto",
    width: "100%"
  }
}));

const ProfilePage = (props: ProfilePageProps) => {
  const classes = useStyles();
  const { userId } = props;
  const [influencer, setInfluencer] = useState<Influencer>({} as any);
  const [openPhoneDialog, setOpenPhoneDialog] = useState(false);
  const [openMailDialog, setOpenMailDialog] = useState(false);
  const [businessHistory, setBusinessHistory] = useState<{ all: Business[], uniq: Business[] }>({ all: [], uniq: [] });
  const [locations, setLocations] = useState<string>("");
  const [isLoading, setIsLoading] = useState(false);
  const [ongoingOffers, setOngoingOffers] = useState([]);

  useEffect(() => {
    const getProfile = async () => {
      setIsLoading(true);
      if (userId) {
        const res = await getInfluencerByUserId(userId);
        if (res.data.influencer[0]) {
          const data = influencerMapper(res.data.influencer[0]);
          const locationString = await getInfluencerTopCities(
            data.id?.toString() ?? ""
          );
          const businesses = await getInfluencerBusinessHistory(
            data.id?.toString() ?? ""
          );
          const offers = await getInfluencerOnGoingOffersById(data.id?.toString() ?? "");
          setOngoingOffers(offers);
          setInfluencer(data);
          setLocations(locationString);
          setBusinessHistory(businesses);
        } else {
          navigate("/app/notFound");
        }
      }
      setIsLoading(false);
    };

    getProfile();
  }, []);

  const navigateToBusinessesPage = (userId) => {
    navigate(`/app/profile/Business/${userId}`);
  };

  const {
    bio,
    categoryName,
    instagramHandleName,
    profilePic,
    fullName,
    instagramProfileLink,
  } = influencer;

  return (
    <div className={classes.root}>
      <UpperVector />
      {isLoading && (
        <div className={classes.progress}>
          <CircularProgress />
        </div>
      )}
      <InfoCard
        {...{
          details: bio ?? "",
          title: fullName ?? "",
          pic: profilePic,
          instaName: instagramHandleName ?? "",
          instaLink: instagramProfileLink,
        }}
      >
        {
          <div className={classes.actions}>
            <IconButton onClick={() => setOpenPhoneDialog(!openMailDialog)}>
              <PhoneOutlinedIcon color="primary" />
            </IconButton>
            <IconButton onClick={() => setOpenMailDialog(!openMailDialog)}>
              <MailOutlineOutlinedIcon color="primary" />
            </IconButton>
          </div>
        }
      </InfoCard>
      <CopyDialog
        text={
          openMailDialog
            ? influencer.user?.email ?? ""
            : influencer.user?.phoneNumber ?? ""
        }
        open={openMailDialog || openPhoneDialog}
        setOpen={openMailDialog ? setOpenMailDialog : setOpenPhoneDialog}
      ></CopyDialog>
      <div style={{ height: "40px" }} />
      <ProfileData
        {...{
          businessAmount: businessHistory.all.length,
          category: categoryName ?? "",
          profileLocation: locations,
          profileName: fullName ?? "",
        }}
      />
      <div style={{ height: "20px" }} />
      {businessHistory.uniq.length > 0 && (
        <ImageList
          style={{ height: "13%" }}
          images={businessHistory.uniq.map(({ profilePic, name, user }) => ({
            src: profilePic ?? "",
            name,
            userId: user?.id ? user.id.toString() : "",
          }))}
          imageStyle={{
            height: "50px",
            width: "50px",
            borderRadius: "50%",
            padding: "5px",
          }}
          title={`Businesses worked with ${fullName}`}
          onClick={navigateToBusinessesPage}
        />
      )}
      {/* {ongoingOffers.length > 0 &&
        <div className={classes.offers}>
          {ongoingOffers.map(({ imgUrl, influencerPartDetails, name }) => {
            return (
              <Offer {...{imgUrl, name, details: influencerPartDetails}} />
            )
          })}
        </div>
      } */}
      <BottomVector />
    </div>
  );
};

export default ProfilePage;