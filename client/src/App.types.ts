export interface View {
    path: string;
    component: JSX.Element;
}

export interface ProfileData {
    _id: string;
    profileName: string;
    profileLocation: string;
    category: string;
}