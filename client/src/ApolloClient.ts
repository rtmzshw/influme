import { ApolloClient, InMemoryCache } from "@apollo/client";
export const GQLClient = new ApolloClient({
  uri: "https://devoted-blowfish-13.hasura.app/v1/graphql",
  cache: new InMemoryCache(),
  headers: {
    "content-type": "application/json",
    "x-hasura-admin-secret":
      "DFJMLfczeSN7OBJRB365PaIGzqrmgIvASS2HxeTIXi3LL1AMs6zD1UkaGMDyt737",
    // Authorization: 'Bearer ' + token
  },
});
